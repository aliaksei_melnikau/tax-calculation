module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    dir: {
      bower_components: 'bower_components',
      build: '<%= dir.bower_components %>/build',
      js: 'src/main/resources/static/js',
      css: 'src/main/resources/static/css'
    },
    
    
    concat: {
      js: {
        src: [
          '<%= dir.bower_components %>/jquery/dist/jquery.js',
          '<%= dir.bower_components %>/bootstrap/dist/js/bootstrap.js',
          '<%= dir.bower_components %>/bootstrap-fileinput/js/fileinput.js',
          '<%= dir.bower_components %>/file-saver/FileSaver.js',
          '<%= dir.bower_components %>/blockUI/jquery.blockUI.js',
          '<%= dir.js %>/account_statement.js'
        ],
        dest: '<%= dir.build %>/tax-calculation.js'
      },
      css: {
        src: [
          '<%= dir.bower_components %>/bootstrap/dist/css/bootstrap.css.map',
          '<%= dir.bower_components %>/bootstrap-fileinput/css/fileinput.css',
          '<%= dir.css %>/statement.css'
        ],
        dest: '<%= dir.build %>/tax-calculation.css'
      }
    },
      
    uglify: {
      main: {
        src: '<%= dir.build %>/tax-calculation.js',
        dest: '<%= dir.js %>/tax-calculation.js'
      },
    },
    
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: '<%= dir.build %>',
          src: [ 'tax-calculation.css' ],
          dest: '<%= dir.css %>',
        }]
      }
    },
    
    watch: {
      scripts: {
        files: [ '<%= dir.js %>/account_statement.js' ],
        tasks: ['concat:js', 'uglify'],
        options: {
          spawn: false,
        },
      }
    },
    
    copy: {
      main: {
        files: [
          { expand: true, cwd: '<%= dir.build %>/', src: ['tax-calculation.js'], dest: '<%= dir.js %>/' },
          { expand: true, cwd: '<%= dir.build %>/', src: ['tax-calculation.css'], dest: '<%= dir.css %>/' }
        ],
      }
    },
  });
  
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  
  grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
  grunt.registerTask('dev', ['concat', 'copy']);
};