package by.helmes.tax.calculation.services.processing;

import by.helmes.tax.calculation.Application;
import by.helmes.tax.calculation.models.payment.UsdPayment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.hamcrest.core.Is.is;

/**
 * @author Aliaksei Melnikau
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class PaymentProcessingServiceTest {

    @Autowired
    private PaymentProcessingService paymentProcessingService;

    public static final String INVOICE_1 = "1\\HELMES2\\LOOTSA 63\\EE\\TALLINN 11415 INVOICE NR.9 ACCORGING CONTRAKT ADMIN-1-4\\2015\\39";
    public static final String INVOICE_2 = "1\\HELMES2\\LOOTSA 63\\EE\\TALLINN 11415 INVOICE NR.I-2015-12 ACCORDIN AGREMENT ADMIN-3-2\\2015\\43";
    public static final String INVOICE_3 = "1\\HELMES2\\LOOTSA 63\\EE\\TALLINN 11415 INVOICE NR 5 ACCORDING AGREEMENT 3 2\\2015\\20";

    @Test
    public void testExtractInvoiceNumberFromSalaryPayment() throws Exception {
        UsdPayment usdPayment_1 = mock(UsdPayment.class);
        when(usdPayment_1.getNazn()).thenReturn(INVOICE_1);

        UsdPayment usdPayment_2 = mock(UsdPayment.class);
        when(usdPayment_2.getNazn()).thenReturn(INVOICE_2);

        UsdPayment usdPayment_3 = mock(UsdPayment.class);
        when(usdPayment_3.getNazn()).thenReturn(INVOICE_3);

        assertThat(paymentProcessingService.extractInvoiceNumberFromSalaryPayment(usdPayment_1),is("9"));
        assertThat(paymentProcessingService.extractInvoiceNumberFromSalaryPayment(usdPayment_2),is("I-2015-12"));
        assertThat(paymentProcessingService.extractInvoiceNumberFromSalaryPayment(usdPayment_3),is("5"));
    }
}