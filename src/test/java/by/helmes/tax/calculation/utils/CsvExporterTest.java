package by.helmes.tax.calculation.utils;

import by.helmes.tax.calculation.Application;
import by.helmes.tax.calculation.models.output.TaxDeclarationEntity;
import by.helmes.tax.calculation.services.output.CsvExporter;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.StringReader;
import java.math.BigDecimal;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by melnikau on 1/29/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class CsvExporterTest {

    @Autowired
    private CsvExporter csvExporter;

    @Test
    public void testGenerateCsvReport() throws Exception {
        TaxDeclarationEntity octoberReport = new TaxDeclarationEntity(2015, Month.OCTOBER);
        octoberReport.setNonSaleRevenue(BigDecimal.valueOf(100.00));
        octoberReport.setTaxBase(BigDecimal.valueOf(500.00));

        TaxDeclarationEntity novemberReport = new TaxDeclarationEntity(2015, Month.NOVEMBER);
        novemberReport.setNonSaleRevenue(BigDecimal.valueOf(200));
        novemberReport.setTaxBase(BigDecimal.valueOf(1100));

        List<TaxDeclarationEntity> reports = new ArrayList<>();
        reports.add(novemberReport);
        reports.add(octoberReport);

        String csvReport = csvExporter.generateCsvReport(reports);

        List<String> reportLines = IOUtils.readLines(new StringReader(csvReport));
        assertEquals("2015,Октябрь,500,100", reportLines.get(1));
        assertEquals("2015,Ноябрь,1100,200", reportLines.get(2));

    }
}