package by.helmes.tax.calculation.utils;

import by.helmes.tax.calculation.Application;
import by.helmes.tax.calculation.utils.props.MessagesResolver;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import by.helmes.tax.calculation.utils.web.UploadedFileValidator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by melnikau on 1/29/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class UploadedFileValidatorTest {

    public static final String BYR_CROPPED_FILE = "src/test/resources/byr_cropped.txt";
    public static final String USD_CROPPED_FILE = "src/test/resources/usd_cropped.txt";
    public static final String TRANSIT_CROPPED_FILE = "src/test/resources/transit_cropped.txt";
    public static final String NOT_STATEMENT_FILE = "src/test/resources/not_statement.txt";

    public static final String BYR_CROPPED_FILE_NAME = "byr_cropped.txt";
    public static final String USD_CROPPED_FILE_NAME = "usd_cropped.txt";
    public static final String TRANSIT_CROPPED_FILE_NAME = "transit_cropped.txt";
    public static final String NOT_STATEMENT_FILE_NAME = "transit_cropped.txt";

    @Autowired
    private UploadedFileValidator uploadedFileValidator;

    @Autowired
    private MessagesResolver messagesResolver;

    @Autowired
    private PropertiesResolver propertiesResolver;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testNoByrUploadedFiles() throws Exception {
        MultipartFile file = getUsdFile();

        expectedEx.expect(RuntimeException.class);
        expectedEx.expectMessage(messagesResolver.getMessage("error.upload.not.byr.statement",getUsdFile().getOriginalFilename()));

        uploadedFileValidator.convertByrStatementToBytes(file);
    }

    @Test
    public void testNotEnoughFilesUploaded() throws Exception {
        MultipartFile[] files = new MultipartFile[] {getUsdFile()};

        expectedEx.expect(RuntimeException.class);
        expectedEx.expectMessage(messagesResolver.getMessage("error.upload.not.enough.usd.files.loaded",
                files.length, propertiesResolver.getNumberOfCurrencyStatements()));

        uploadedFileValidator.convertUsdStatementsToBytes(files);
    }

    @Test
    public void testNotStatementUploaded() throws Exception {
        MultipartFile[] files = new MultipartFile[] {getUsdFile(),getNotStatementFile()};

        expectedEx.expect(RuntimeException.class);
        expectedEx.expectMessage(messagesResolver.getMessage("error.upload.not.usd.statement", getNotStatementFile().getOriginalFilename()));

        uploadedFileValidator.convertUsdStatementsToBytes(files);
    }

    private byte[] readBytesOfTestFile(String fullPath) throws IOException {
        return Files.readAllBytes(Paths.get(fullPath));
    }

    private MultipartFile getByrFile() throws Exception {
        MultipartFile multipartFileByr = mock(MultipartFile.class);
        when(multipartFileByr.getBytes()).thenReturn(readBytesOfTestFile(BYR_CROPPED_FILE));
        when(multipartFileByr.getOriginalFilename()).thenReturn(BYR_CROPPED_FILE_NAME);

        return multipartFileByr;
    }

    private MultipartFile getUsdFile() throws Exception {
        MultipartFile multipartFileUsd = mock(MultipartFile.class);
        when(multipartFileUsd.getBytes()).thenReturn(readBytesOfTestFile(USD_CROPPED_FILE));
        when(multipartFileUsd.getOriginalFilename()).thenReturn(USD_CROPPED_FILE_NAME);

        return multipartFileUsd;
    }

    private MultipartFile getTransitFile() throws Exception {
        MultipartFile multipartFileTransit = mock(MultipartFile.class);
        when(multipartFileTransit.getBytes()).thenReturn(readBytesOfTestFile(TRANSIT_CROPPED_FILE));
        when(multipartFileTransit.getOriginalFilename()).thenReturn(TRANSIT_CROPPED_FILE_NAME);

        return multipartFileTransit;
    }

    private MultipartFile getNotStatementFile() throws Exception {
        MultipartFile notStatementFile = mock(MultipartFile.class);
        when(notStatementFile.getBytes()).thenReturn(readBytesOfTestFile(NOT_STATEMENT_FILE));
        when(notStatementFile.getOriginalFilename()).thenReturn(NOT_STATEMENT_FILE_NAME);

        return notStatementFile;
    }
}