$(function() {
    initFileUpload('#tax_statement_rub');
    initFileUpload('#ledger_statement_usd');
    initFileUpload('#tax_statement_trans');
    initFileUpload('#ledger_statement_rub');
    initFileUpload('#tax_statement_usd');
    initFileUpload('#ledger_statement_trans');
    initFileUpload('#ledger',true);
});

$( "#statementSubmitter" ).click(function() {
    var formData = new FormData();

    if ($('.close')) {
        $('.close').parent().removeClass('in');
    }

    $.blockUI({ message: '<h1><img src="images/cropped.gif" />Создаем отчет...</h1>' });

    sendPostRequestWithFiles("/helmes-tax/report", "tax", "report.xls", "application/vnd.ms-excel;charset=utf-8");

});

$( "#generateLedger" ).click(function() {

    if ($('.close')) {
        $('.close').parent().removeClass('in');
    }

    $.blockUI({ message: '<h1><img src="images/cropped.gif" />Заполняем Книгу...</h1>' });
    
    sendPostRequestWithFiles("/helmes-tax/ledger", "ledger", "ledger.xls", "application/vnd.ms-excel;charset=utf-8");
});

function sendPostRequestWithFiles(url, id_prefix, fileName, mimeType){
    var formData = new FormData();
    formData.append("statement_byr", $("#"+id_prefix+"_statement_rub").prop("files")[0]);
    formData.append("statement_usd", $("#"+id_prefix+"_statement_usd").prop("files")[0]);
    formData.append("statement_usd", $("#"+id_prefix+"_statement_trans").prop("files")[0]);
    formData.append("include_sending_fee", $("#"+id_prefix+"_statement_sending_fee").is(':checked'));
    if (id_prefix=='ledger') {
        formData.append("ledger", $("#ledger").prop("files")[0]);
        formData.append("ledger_possession", $('#ledger_possession').is(':checked'));
    }

    var request = new XMLHttpRequest();

    request.onreadystatechange = function (oEvent) {
        if (request.readyState === 4) {
            $.unblockUI();
            if (request.status === 200) {
                var blobObject = new Blob([binaryStringToArray(this.response)], {type: mimeType});
                saveAs(blobObject, fileName);
            } else {
                $('#statement_error span').text(decodeURIComponent(request.getResponseHeader("processingError")));
                $('#statement_error').addClass('in');
                console.log("Error", request.getResponseHeader("processingError"));
            }
        }
    };

    request.addEventListener("error", transferFailed);
    request.addEventListener("abort", transferCanceled);

    request.open("POST", url);
    request.overrideMimeType('text\/plain; charset=x-user-defined');
    request.send(formData);
}

$('.close').click(function () {
    $(this).parent().removeClass('in');
});

$('#ledger_possession').change(function () {
    if ($('#ledger_possession').is(':checked')) {
        $('#ledger').fileinput('enable');
    } else {
        $('#ledger').fileinput('clear');
        $('#ledger').fileinput('disable');
    }
});

function binaryStringToArray(str) {
    var arr = new Uint8Array(str.length);
    for (var i = 0; i < str.length; i++) {
        arr[i] = str.charCodeAt(i) & 0xff;
    }
    return arr;
}

function transferFailed(evt) {
    $.unblockUI();
    console.log("An error occurred while transferring the file.");
}

function transferCanceled(evt) {
    $.unblockUI();
    console.log("The transfer has been canceled by the user.");
}

function initFileUpload(selector,disable) {
    $(selector).fileinput({
        showUpload: false,
        showPreview: false,
        maxFileCount: 1
    });
    if (disable) {
        $(selector).fileinput('disable');
    }
}