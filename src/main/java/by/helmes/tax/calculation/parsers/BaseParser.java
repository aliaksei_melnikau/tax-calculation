package by.helmes.tax.calculation.parsers;

import java.time.format.DateTimeFormatter;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import by.helmes.tax.calculation.models.payment.BasePayment;

/**
 * Base parser implementation that parses file content in Prior Online Banking
 * format.
 * 
 * @author petr
 */
@Component
public abstract class BaseParser<T extends BasePayment> {
    private static final String ENTRY_SEPARATOR = "=";
    private Pattern entryPattern;
    // TODO move format in config
    protected static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public BaseParser() {
        // TODO move pattern in config
        this.entryPattern = Pattern.compile("(\\^)([^\\^]+?=[^\\^]+?)(\\^)", Pattern.DOTALL);
    }

    protected List<SimpleEntry<String, String>> parseFileEntities(String fileContent) {
        List<SimpleEntry<String, String>> entries = new ArrayList<SimpleEntry<String, String>>();
        Matcher matcher = entryPattern.matcher(fileContent);
        while (matcher.find()) {
            String entryData[] = matcher.group(2).split(ENTRY_SEPARATOR);
            entries.add(new SimpleEntry<String, String>(entryData[0], entryData[1]));
        }
        return entries;
    }

    abstract List<T> getPayments(String fileContent);
}
