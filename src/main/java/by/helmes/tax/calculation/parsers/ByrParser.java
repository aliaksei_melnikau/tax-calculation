package by.helmes.tax.calculation.parsers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import by.helmes.tax.calculation.models.payment.ByrPayment;
import by.helmes.tax.calculation.utils.exception.TaxException;
import by.helmes.tax.calculation.utils.props.MessagesResolver;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Parser for BYR statements.
 * 
 * @author petr
 * 
 */
@Component
public class ByrParser extends BaseParser<ByrPayment> {

    @Autowired
    private PropertiesResolver propertiesResolver;

    @Autowired
    private MessagesResolver messagesResolver;

    @Override
    public List<ByrPayment> getPayments(String fileContent) {
        List<SimpleEntry<String, String>> entities = parseFileEntities(fileContent);
        List<ByrPayment> payments = new ArrayList<ByrPayment>();
        ByrPayment currentPayment = null;
        //TODO find implementation for Finite State Machine 
        for (Entry<String, String> entry : entities) {
            switch (entry.getKey()) {
            case "DocDate":
                if (currentPayment != null) {
                    payments.add(currentPayment);
                }
                currentPayment = new ByrPayment();
                currentPayment.setDocDate(LocalDate.parse(entry.getValue(), DATE_FORMATTER));
                break;
            case "Credit":
                currentPayment.setCredit(new BigDecimal(entry.getValue()));
                break;
            case "DocSourceID":
                currentPayment.setDocSourceId(Long.valueOf(entry.getValue()));
                break;
            case "KorName":
                currentPayment.setKorName(entry.getValue());
                break;
            case "Nazn":
                currentPayment.setNazn(entry.getValue());
                break;
            }
        }
        if (currentPayment != null) {
            payments.add(currentPayment);
        }
        updateByrPaymentsWithBankFee(payments);
        return payments;
    }

    private void updateByrPaymentsWithBankFee(List<ByrPayment> payments) {
        for (ByrPayment byrPayment : payments) {
            if (byrPayment.getNazn()!=null && byrPayment.getNazn().contains(propertiesResolver.getNaznObligatoryCurrencySellingKeywordByr()) &&
                    byrPayment.getNazn().toLowerCase().contains(propertiesResolver.getPaymentProcessingByrFeeKeyword())) {
                BigDecimal bankFee = extractBankFee(byrPayment);
                validateExtractedFeeValue(byrPayment, bankFee);
                byrPayment.setCredit(byrPayment.getCredit().add(bankFee));
            }
        }
    }

    private BigDecimal extractBankFee(ByrPayment byrPayment) {
        StringBuilder feeAmount = new StringBuilder();
        int index = byrPayment.getNazn().toLowerCase().lastIndexOf(propertiesResolver.getPaymentProcessingByrFeeKeyword());
        index += propertiesResolver.getPaymentProcessingByrFeeKeyword().length(); // we need digits after word "комиссия банка"
        for (; index < byrPayment.getNazn().length(); index++) {
            char ch = byrPayment.getNazn().charAt(index);
            if ( Character.isDigit(ch) || ('.' == ch && !feeAmount.toString().contains(".")))  {
                feeAmount.append(ch);
                continue;
            }
            if (Character.isWhitespace(ch)) {
                continue;
            }
            if (!Character.isDigit(ch)) {
                break;
            }
        }
        return new BigDecimal(feeAmount.toString());
    }

    // extracted fee should be 0.2%
    private void validateExtractedFeeValue(ByrPayment byrPayment, BigDecimal extractedFee) {
        BigDecimal extractedFeePercentage = extractedFee.divide(byrPayment.getCredit().add(extractedFee), 3, RoundingMode.HALF_UP);
        extractedFeePercentage = extractedFeePercentage.multiply(BigDecimal.valueOf(100));
        if (!(BigDecimal.valueOf(propertiesResolver.getObligatorySellingBankFeePercent()).compareTo(extractedFeePercentage) == 0)) {
            throw new TaxException(messagesResolver.getMessage("error.payment.parsing.no.fee.extracted", byrPayment.getNazn(),propertiesResolver.getObligatorySellingBankFeePercent()));
        }
    }

}
