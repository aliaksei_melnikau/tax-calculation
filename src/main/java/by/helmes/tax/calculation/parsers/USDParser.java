package by.helmes.tax.calculation.parsers;

import by.helmes.tax.calculation.models.payment.UsdPayment;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 * Parser for USD statements.
 *
 * @author petr
 */
@Component
public class USDParser extends BaseParser<UsdPayment> {

    @Override
    public List<UsdPayment> getPayments(String fileContent) {
        List<SimpleEntry<String, String>> entities = parseFileEntities(fileContent);
        List<UsdPayment> payments = new ArrayList<UsdPayment>();
        UsdPayment currentPayment = null;
        //TODO find implementation for Finite State Machine 
        for (Entry<String, String> entry : entities) {
            switch (entry.getKey()) {
                case "DocDate":
                    if (currentPayment != null) {
                        payments.add(currentPayment);
                    }
                    currentPayment = new UsdPayment();
                    currentPayment.setDocDate(LocalDate.parse(entry.getValue(), DATE_FORMATTER));
                    break;
                case "Deb":
                    currentPayment.setDeb(new BigDecimal(entry.getValue()));
                    break;
                case "Cre":
                    currentPayment.setCre(new BigDecimal(entry.getValue()));
                    break;
                case "CreQ":
                    currentPayment.setCreQ(new BigDecimal(entry.getValue()));
                    break;
                case "DebQ":
                    currentPayment.setDebQ(new BigDecimal(entry.getValue()));
                    break;
                case "InDocID":
                    currentPayment.setInDocId(Long.valueOf(entry.getValue()));
                    break;
                case "KorName":
                    currentPayment.setKorName(entry.getValue());
                    break;
                case "Nazn":
                    currentPayment.setNazn(entry.getValue());
                    break;
            }
        }
        if (currentPayment != null) {
            payments.add(currentPayment);
        }
        return payments;
    }

}
