package by.helmes.tax.calculation.utils.xls.ledger;

import by.helmes.tax.calculation.utils.exception.TaxException;
import by.helmes.tax.calculation.services.processing.LedgerEntitiesProcessor;
import by.helmes.tax.calculation.utils.props.MessagesResolver;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by aliaksei.melnikau on 21.03.16.
 */
public class LedgerXlsUtils {

    @Autowired
    protected PropertiesResolver propertiesResolver;

    @Autowired
    protected MessagesResolver messagesResolver;

    @Autowired
    protected LedgerEntitiesProcessor ledgerEntitiesProcessor;

    /**
     *
     * Returns index of a row, which contains numbers for each header
     *
     * @param sheet - sheet from ledger book which has row with numbered headers
     * @return index of a row, which contains numbers for each header. If no such row found -1 is returned.
     */
    public int getRowNumberWithHeaderNumbers(HSSFSheet sheet) {
        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = null;
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            Iterator<Cell> cellIterator = currentRow.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC && Double.valueOf(cell.getNumericCellValue()).equals(Double.valueOf(1d))) {
                    if (cellIterator.hasNext()) {
                        Cell nextCell = cellIterator.next();
                        if (nextCell.getCellType() == Cell.CELL_TYPE_NUMERIC && Double.valueOf(nextCell.getNumericCellValue()).equals(Double.valueOf(2d))) {
                            return currentRow.getRowNum();
                        }
                    }
                }
            }
        }
        throw new TaxException(messagesResolver.getMessage("error.ledger.xls.no.numeric.row",sheet.getSheetName()));
    }

    /**
     * copies row to the specified location (by index)
     *
     * @param worksheet - ledger sheet to which we want add a new row
     * @param sourceRowNum - index of the row that should be copied
     * @param destinationRowNum - index of the row, where copy should be placed
     *
     */
    public void copyRow(HSSFSheet worksheet, int sourceRowNum, int destinationRowNum) {
        // Get the source / new row
        HSSFRow newRow = worksheet.getRow(destinationRowNum);
        HSSFRow sourceRow = worksheet.getRow(sourceRowNum);

        // If the row exist in destination, push down all rows by 1 else create a new row
        if (newRow != null) {
            worksheet.shiftRows(destinationRowNum, worksheet.getLastRowNum(), 1);
        } else {
            newRow = worksheet.createRow(destinationRowNum);
        }

        // Loop through source columns to add to new row
        for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
            // Grab a copy of the old/new cell
            HSSFCell oldCell = sourceRow.getCell(i);
            HSSFCell newCell = newRow.createCell(i);

            // If the old cell is null jump to next cell
            if (oldCell == null) {
                continue;
            }

            newCell.setCellStyle(oldCell.getCellStyle());

            // If there is a cell comment, copy
            if (oldCell.getCellComment() != null) {
                newCell.setCellComment(oldCell.getCellComment());
            }

            // If there is a cell hyperlink, copy
            if (oldCell.getHyperlink() != null) {
                newCell.setHyperlink(oldCell.getHyperlink());
            }

            // Set the cell data type
            newCell.setCellType(oldCell.getCellType());

            // Set the cell data value
            switch (oldCell.getCellType()) {
                case Cell.CELL_TYPE_BLANK:
                    newCell.setCellValue(oldCell.getStringCellValue());
                    break;
                case Cell.CELL_TYPE_BOOLEAN:
                    newCell.setCellValue(oldCell.getBooleanCellValue());
                    break;
                case Cell.CELL_TYPE_ERROR:
                    newCell.setCellErrorValue(oldCell.getErrorCellValue());
                    break;
                case Cell.CELL_TYPE_FORMULA:
                    newCell.setCellFormula(oldCell.getCellFormula());
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    newCell.setCellValue(oldCell.getNumericCellValue());
                    break;
                case Cell.CELL_TYPE_STRING:
                    newCell.setCellValue(oldCell.getRichStringCellValue());
                    break;
            }
        }

        // If there are are any merged regions in the source row, copy to new row
        for (int i = 0; i < worksheet.getNumMergedRegions(); i++) {
            CellRangeAddress cellRangeAddress = worksheet.getMergedRegion(i);
            if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
                CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(),
                        (newRow.getRowNum() +
                                (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow()
                                )),
                        cellRangeAddress.getFirstColumn(),
                        cellRangeAddress.getLastColumn());
                worksheet.addMergedRegion(newCellRangeAddress);
            }
        }
    }

    /**
     *
     * Adjusts row heights to expand rows with zero height
     *
     * @param sheet - sheet from ledger book
     */
    public void adjustRowHeights(HSSFSheet sheet) {
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            rowIterator.next().setZeroHeight(false);
        }
    }

    protected int getColumnIndex(Row row, double columnHeaderValue) {
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            Cell currentCell = cellIterator.next();
            if (currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC && Double.valueOf(currentCell.getNumericCellValue()).equals(columnHeaderValue)) {
                return currentCell.getColumnIndex();
            }
        }
        return -1;
    }


    protected int getColumnIndex(Row row, ColumnType columnType, Map<ColumnType,Integer> columnIndexMap) {
        if (columnIndexMap.get(columnType) != null) {
            return columnIndexMap.get(columnType);
        }
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            Cell currentCell = cellIterator.next();
            if (currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC && Double.valueOf(currentCell.getNumericCellValue()).equals(getHeaderColumnValue(columnType))) {
                columnIndexMap.put(columnType, currentCell.getColumnIndex());
                return currentCell.getColumnIndex();
            }
        }
        return -1;
    }

    protected Double getHeaderColumnValue(ColumnType columnType) {
        switch (columnType) {
            case DATE: return propertiesResolver.getLedgerSection1Part1DateColumnName();
            case DOCUMENT: return propertiesResolver.getLedgerSection1Part1DocumentColumnName();
            case OPERATION: return propertiesResolver.getLedgerSection1Part1OperationColumnName();
            case REVENUE_VALUE:return propertiesResolver.getLedgerSection1Part1RevenueColumnName();
            case NON_REVENUE_VALUE:return propertiesResolver.getLedgerSection1Part1NonRevenueColumnName();
            case SHIPMENT_DATE: return propertiesResolver.getLedgerSection1Part2ShipmentDateColumnName();
            case RECEIVER: return propertiesResolver.getLedgerSection1Part2ReceiverColumnName();
            case AMOUNT_BYR: return propertiesResolver.getLedgerSection1Part2ShipmentCostForMonthByrColumnName();
            case CURRENCY_NAME:return propertiesResolver.getLedgerSection1Part2ShipmentCurrencyColumnName();
            case AMOUNT_CURRENCY:return propertiesResolver.getLedgerSection1Part2ShipmentCostForMonthCurrencyColumnName();
            case FIRST_MONTH_INDEX:return propertiesResolver.getLedgerSection1Part2FirstMonthColumnName();
            case LAST_MONTH_INDEX:return propertiesResolver.getLedgerSection1Part2LastMonthColumnName();
            case LAST_MANUALLY_FILLED_COLUMN:return propertiesResolver.getLedgerSection1Part2LastManuallyFilledColumnName();
            default: throw new IllegalArgumentException("Unsupported column type");
        }
    }

    public enum ColumnType {
        DATE, DOCUMENT, OPERATION, REVENUE_VALUE, NON_REVENUE_VALUE,
        SHIPMENT_DATE, RECEIVER, AMOUNT_BYR, CURRENCY_NAME, AMOUNT_CURRENCY, FIRST_MONTH_INDEX, LAST_MONTH_INDEX, LAST_MANUALLY_FILLED_COLUMN
    }
}
