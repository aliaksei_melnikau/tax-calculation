package by.helmes.tax.calculation.utils.xls.ledger;

import by.helmes.tax.calculation.models.output.Quarter;
import by.helmes.tax.calculation.models.output.Section1Part1Entity;
import by.helmes.tax.calculation.utils.exception.TaxException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * Created by aliaksei.melnikau on 18.03.16.
 */
@Component
public class LedgerSection1Part1XlsUtils extends LedgerXlsUtils {

    /**
     * This implementation assumes that tax payer files declaration once in a quarter(4 times per year). <br>
     * We remove extra rows as by default ledger book is designed for 12 periods and we need only 4
     *
     * @param sheet - sheet from ledger book which name is "Раздел I ч.I"
     */
    public void removeRowsExceedingForthQuarter(HSSFSheet sheet) {
        Iterator<Row> rowIterator = sheet.rowIterator();
        int quarterCount = 0;
        Row currentRow = null;
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            Iterator<Cell> cellIterator = currentRow.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().equals(propertiesResolver.getLedgerSection1Part1YearHeader())) {
                    quarterCount++;
                }
            }
            // the rest of that page will be deleted as we need only 4 quarters per year
            if (quarterCount == Quarter.values().length) {
                break;
            }
        }
        //remove remaining rows
        if (currentRow != null) {
            for (int rowIndex = currentRow.getRowNum() + 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
                if (sheet.getRow(rowIndex)!=null) {
                    sheet.removeRow(sheet.getRow(rowIndex));
                }
            }
        }
    }

    private int getQuarterFirstFillableRowIndex(HSSFSheet sheet, Quarter quarter) {
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);

        Quarter currentQuarter = Quarter.I;
        Row currentRow = null;
        boolean isManuallyFillableRow = true;

        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();

            if (currentRow.getRowNum() <= indexOfRowWhichHasHeaderNumbers) {
                continue;
            }

            if (!isManuallyFillableRow && !isAutomaticallyFilledAccumulatedResultsRow(currentRow)) {
                currentQuarter = currentQuarter.getNextQuarter();
            }
            isManuallyFillableRow = !isAutomaticallyFilledAccumulatedResultsRow(currentRow);
            if (isManuallyFillableRow && quarter == currentQuarter) {
                return currentRow.getRowNum();
            }
        }
        throw new TaxException(messagesResolver.getMessage("error.ledger.xls.section1.part1.no.quarter.row",sheet.getSheetName(),quarter));
    }

    private boolean isAutomaticallyFilledAccumulatedResultsRow(Row row) {
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if ((cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().equals(propertiesResolver.getLedgerSection1Part1YearHeader()))
                    || (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().equals(propertiesResolver.getLedgerSection1Part1QuarterHeader()))) {
                return true;
            }
        }
        return false;
    }

    private int getQuarterLastFillableRowIndex(HSSFSheet sheet, Quarter quarter) {
        int quarterFirstFillableRowIndex = getQuarterFirstFillableRowIndex(sheet, quarter);
        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = null;
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();

            if (currentRow.getRowNum() <= quarterFirstFillableRowIndex) {
                continue;
            }

            if (isAutomaticallyFilledAccumulatedResultsRow(currentRow)) {
                return currentRow.getRowNum()-1;
            }
        }
        throw new TaxException(messagesResolver.getMessage("error.ledger.xls.section1.part1.no.quarter.row",sheet.getSheetName(),quarter));
    }

    /**
     * Clears existing records for excel sheet with name "Раздел I ч.I".
     *
     * @param sheet - sheet from ledger book which name is "Раздел I ч.I"
     */
    public void clearExistingRecords(HSSFSheet sheet) {
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);

        int lastNullableCellIndex = getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.NON_REVENUE_VALUE, new HashMap<>());
        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = null;
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            // do not clear anything above headers
            if (currentRow.getRowNum() <= indexOfRowWhichHasHeaderNumbers) {
                continue;
            }
            Iterator<Cell> cellIterator = currentRow.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if ((cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().equals(propertiesResolver.getLedgerSection1Part1YearHeader()))
                        || (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().equals(propertiesResolver.getLedgerSection1Part1QuarterHeader()))) {
                    break;
                }
                // do not clear cells after non-sale-revenue column, as those columns are pre-filled with default values
                if (cell.getColumnIndex() > lastNullableCellIndex) {
                    break;
                }
                cell.setCellType(Cell.CELL_TYPE_BLANK);
            }
        }
    }

    /**
     * Extracts existing records from excel sheet with name "Раздел I ч.I".
     *
     * @param sheet - sheet from ledger book which name is "Раздел I ч.I"
     *
     * @return List of existing records for given excel sheet
     */
    public List<Section1Part1Entity> extractExistingSection1Part1Entries(HSSFSheet sheet) {
        List<Section1Part1Entity> records = new ArrayList<>();
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);

        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = null;
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            // actual data can only be after row with header numbers
            if (currentRow.getRowNum() <= indexOfRowWhichHasHeaderNumbers) {
                continue;
            }
            Map<ColumnType, Integer> columnIndexMap = new HashMap<>();
            Cell dateCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.DATE, columnIndexMap));
            Cell revenueCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.REVENUE_VALUE, columnIndexMap));
            Cell nonRevenueCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.NON_REVENUE_VALUE, columnIndexMap));
            Cell documentNameCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.DOCUMENT, columnIndexMap));
            Cell operationNameCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.OPERATION, columnIndexMap));

            if (dateCell.getCellType() == Cell.CELL_TYPE_NUMERIC && DateUtil.isCellDateFormatted(dateCell) && (revenueCell.getCellType() != Cell.CELL_TYPE_BLANK || nonRevenueCell.getCellType() != Cell.CELL_TYPE_BLANK)) {
                Date recordInsertionDate = DateUtil.getJavaDate(dateCell.getNumericCellValue());
                LocalDate recordInsertionLocalDate = recordInsertionDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                BigDecimal amount = null;
                Section1Part1Entity.RevenueType revenueType = null;
                if (revenueCell.getCellType() != Cell.CELL_TYPE_BLANK) {
                    amount = BigDecimal.valueOf(revenueCell.getNumericCellValue());
                    revenueType = Section1Part1Entity.RevenueType.SALARY;
                } else {
                    amount = BigDecimal.valueOf(nonRevenueCell.getNumericCellValue());
                    revenueType = Section1Part1Entity.RevenueType.NON_SALE;
                }
                Section1Part1Entity section1Part1Entity = new Section1Part1Entity(revenueType, recordInsertionLocalDate, amount);
                if (documentNameCell.getCellType() == Cell.CELL_TYPE_STRING) {
                    section1Part1Entity.setDocumentName(documentNameCell.getStringCellValue());
                }
                if (operationNameCell.getCellType() == Cell.CELL_TYPE_STRING) {
                    section1Part1Entity.setOperationName(operationNameCell.getStringCellValue());
                }
                records.add(section1Part1Entity);
            }
        }
        return records;
    }

    public void writeSection1Part1Entities(HSSFSheet sheet, List<Section1Part1Entity> newSection1Part1Entities) {
        List<Section1Part1Entity> existingEntities = extractExistingSection1Part1Entries(sheet);
        clearExistingRecords(sheet);
        removeRowsExceedingForthQuarter(sheet);
        List<Section1Part1Entity> mergedEntities = ledgerEntitiesProcessor.mergeSection1Part1Entities(existingEntities, newSection1Part1Entities);
        checkIfEntitiesHasTheSameYear(mergedEntities);
        Collections.sort(mergedEntities);
        for (Quarter quarter : Quarter.values()) {
            writeSection1Part1EntitiesForQuarter(sheet,mergedEntities,quarter);
        }
        adjustRowHeights(sheet);
    }

    private void writeSection1Part1EntitiesForQuarter(HSSFSheet sheet, List<Section1Part1Entity> section1Part1Entities, Quarter quarter) {
        int quarterFirstFillableRowIndex = getQuarterFirstFillableRowIndex(sheet, quarter);
        int quarterLastFillableRowIndex = getQuarterLastFillableRowIndex(sheet, quarter);
        int numberOfAvailibleRows = quarterLastFillableRowIndex - quarterFirstFillableRowIndex + 1;
        List<Section1Part1Entity> section1Part1EntitiesForQuarter = ledgerEntitiesProcessor.getSection1Part1EntitiesForQuarter(section1Part1Entities, quarter);
        // add missing rows if needed
        if (section1Part1EntitiesForQuarter.size() > numberOfAvailibleRows) {
            int rowsMissing = section1Part1EntitiesForQuarter.size() - numberOfAvailibleRows;
            for (int i = quarterLastFillableRowIndex; rowsMissing > 0; i++, rowsMissing--) {
                copyRow(sheet,quarterFirstFillableRowIndex,quarterLastFillableRowIndex);
            }
        }
        // write data to sheet
        for (Section1Part1Entity section1Part1Entity : section1Part1EntitiesForQuarter) {
            writeSingleSection1Part1Entity(sheet,section1Part1Entity,quarterFirstFillableRowIndex++);
        }
    }

    private void writeSingleSection1Part1Entity(HSSFSheet sheet, Section1Part1Entity section1Part1Entity, int rowIndex) {
        Row currentRow = sheet.getRow(rowIndex);
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);

        Map<ColumnType, Integer> columnIndexMap = new HashMap<>();
        Cell dateCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.DATE, columnIndexMap));
        Cell revenueCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.REVENUE_VALUE, columnIndexMap));
        Cell nonRevenueCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.NON_REVENUE_VALUE, columnIndexMap));
        Cell documentNameCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.DOCUMENT, columnIndexMap));
        Cell operationNameCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.OPERATION, columnIndexMap));

        dateCell.setCellValue(DateUtil.getExcelDate(java.sql.Date.valueOf(section1Part1Entity.getDate())));
        documentNameCell.setCellValue(section1Part1Entity.getDocumentName());
        operationNameCell.setCellValue(section1Part1Entity.getOperationName());
        if (section1Part1Entity.getType() == Section1Part1Entity.RevenueType.SALARY) {
            revenueCell.setCellValue(section1Part1Entity.getAmountByr().doubleValue());
        } else {
            nonRevenueCell.setCellValue(section1Part1Entity.getAmountByr().doubleValue());
        }
    }

    private void checkIfEntitiesHasTheSameYear(List<Section1Part1Entity> mergedEntities) {
        if (mergedEntities.size() < 2) {
            return;
        }
        int year = mergedEntities.get(0).getDate().getYear();
        for (Section1Part1Entity section1Part1Entity : mergedEntities) {
            if (section1Part1Entity.getDate().getYear() != year) {
                throw new TaxException(messagesResolver.getMessage("error.ledger.xls.section1.part1.records.different.year",
                        String.valueOf(year),String.valueOf(section1Part1Entity.getDate().getYear())));
            }
        }
    }
}
