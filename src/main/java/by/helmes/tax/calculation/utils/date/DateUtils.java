package by.helmes.tax.calculation.utils.date;

import by.helmes.tax.calculation.utils.props.MessagesResolver;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Created by aliaksei.melnikau on 22.03.16.
 */
@Component
public class DateUtils {

    @Autowired
    private PropertiesResolver propertiesResolver;

    @Autowired
    private MessagesResolver messagesResolver;

    private volatile DateTimeFormatter dateTimeFormatter;

    public DateTimeFormatter getDateFormatter() {
        if (dateTimeFormatter == null) {
            synchronized (this) {
                dateTimeFormatter = DateTimeFormatter.ofPattern(propertiesResolver.getLedgerDateFormat());
            }
        }
        return dateTimeFormatter;
    }


    public LocalDate getLastDayOfPreviousMonth(LocalDate operationDate) {
        LocalDate previousMonth = operationDate.minus(1, ChronoUnit.MONTHS);
        return previousMonth.withDayOfMonth(previousMonth.lengthOfMonth());
    }

    public LocalDate parseDate(String dateString) {
        return LocalDate.parse(dateString, getDateFormatter());
    }

    public String getMonthName(Month month) {
        switch (month) {
            case JANUARY:
                return messagesResolver.getMessage("report.csv.month.january");
            case FEBRUARY:
                return messagesResolver.getMessage("report.csv.month.february");
            case MARCH:
                return messagesResolver.getMessage("report.csv.month.march");
            case APRIL:
                return messagesResolver.getMessage("report.csv.month.april");
            case MAY:
                return messagesResolver.getMessage("report.csv.month.may");
            case JUNE:
                return messagesResolver.getMessage("report.csv.month.june");
            case JULY:
                return messagesResolver.getMessage("report.csv.month.july");
            case AUGUST:
                return messagesResolver.getMessage("report.csv.month.august");
            case SEPTEMBER:
                return messagesResolver.getMessage("report.csv.month.september");
            case OCTOBER:
                return messagesResolver.getMessage("report.csv.month.october");
            case NOVEMBER:
                return messagesResolver.getMessage("report.csv.month.november");
            case DECEMBER:
                return messagesResolver.getMessage("report.csv.month.december");
            default:
                throw new RuntimeException(messagesResolver.getMessage("report.csv.error.incorrect.month", month));
        }
    }
}
