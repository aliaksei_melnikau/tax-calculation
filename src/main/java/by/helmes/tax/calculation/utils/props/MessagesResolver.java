package by.helmes.tax.calculation.utils.props;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by melnikau on 1/29/16.
 */
@Component
public class MessagesResolver {

    @Autowired
    private ApplicationContext context;

    public String getMessage(String key, Object... args) {
        return context.getMessage(key,args,null);
    }

    public String getMessage(String key, Locale locale, Object... args) {
        return context.getMessage(key,args,locale);
    }
}
