package by.helmes.tax.calculation.utils.math;

import org.springframework.stereotype.Component;

/**
 * @author Aliaksei Melnikau
 */

@Component
public class MathUtils {
    /**
     * Compares each word in array with given <b>baseWord</b> by Levenstein algorithm. The most similar or the same one is returned.<br>
     * When similarity threshold is exceeded (default value is 3) null is returned.
     *
     * @param baseWord word with which <b>candidateWords</b> will be compared
     * @param candidateWords array of words that to be compared with <b>baseWord</b>.
     *
     * @return The most similar word or null (when similarity threshold is exceeded)
     */
    public String findSimilarWord(String baseWord, String[] candidateWords) {
        int shortestLevensteinDistance = Integer.MAX_VALUE;
        String closestMatch=null;
        for (String word : candidateWords) {
            int levensteinDistance = levenshteinDistance(word, baseWord);
            if (levensteinDistance == 0) {
                return word;
            }
            if (levensteinDistance < shortestLevensteinDistance) {
                shortestLevensteinDistance = levensteinDistance;
                closestMatch = word;
            }
        }
        if (shortestLevensteinDistance <= 3) {
            return closestMatch;
        }
        return null;
    }

    private  int levenshteinDistance (CharSequence lhs, CharSequence rhs) {
        int len0 = lhs.length() + 1;
        int len1 = rhs.length() + 1;

        // the array of distances
        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        // initial cost of skipping prefix in String s0
        for (int i = 0; i < len0; i++) cost[i] = i;

        // dynamically computing the array of distances

        // transformation cost for each letter in s1
        for (int j = 1; j < len1; j++) {
            // initial cost of skipping prefix in String s1
            newcost[0] = j;

            // transformation cost for each letter in s0
            for(int i = 1; i < len0; i++) {
                // matching current letters in both strings
                int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;

                // computing cost for each transformation
                int cost_replace = cost[i - 1] + match;
                int cost_insert  = cost[i] + 1;
                int cost_delete  = newcost[i - 1] + 1;

                // keep minimum cost
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }

            // swap cost/newcost arrays
            int[] swap = cost; cost = newcost; newcost = swap;
        }

        // the distance is the cost for transforming all letters in both strings
        return cost[len0 - 1];
    }
}
