package by.helmes.tax.calculation.utils.web;

import by.helmes.tax.calculation.utils.exception.TaxException;
import by.helmes.tax.calculation.utils.props.MessagesResolver;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by melnikau on 1/29/16.
 */
@Component
public class UploadedFileValidator {

    @Autowired
    private MessagesResolver messagesResolver;

    @Autowired
    private PropertiesResolver propertiesResolver;

    /**
     *
     * Converts USD (main and transit) statements into bytes and performs validation
     *
     * @param files MultipartFile[] of USD statements
     * @return raw bytes[] for each USD bank statement stored in a List
     * @throws IOException
     */
    public List<byte[]> convertUsdStatementsToBytes(MultipartFile[] files) throws IOException {
        validateUsdStatements(files);
        List<byte[]> usdStatementsBytes = new ArrayList<>();
        for (MultipartFile file : files) {
            usdStatementsBytes.add(file.getBytes());
        }
        return usdStatementsBytes;
    }

    private void validateUsdStatements(MultipartFile[] files) throws IOException {
        if (files == null) {
            throw new TaxException(messagesResolver.getMessage("error.upload.no.usd.files.data"));
        }
        if (files.length != propertiesResolver.getNumberOfCurrencyStatements()) {
            throw new TaxException(messagesResolver.getMessage("error.upload.not.enough.usd.files.loaded",
                    files.length, propertiesResolver.getNumberOfCurrencyStatements()));
        }
        emptyFileContentValidation(files);
        for (MultipartFile file : files) {
            StatementType statementType = getStatementType(IOUtils.toString(file.getBytes(), propertiesResolver.getStatementEncoding()));
            if (statementType != StatementType.USD) {
                throw new RuntimeException(messagesResolver.getMessage("error.upload.not.usd.statement",file.getOriginalFilename()));
            }
        }
    }

    /**
     *
     * Converts BYR bank statement into bytes and performs validation
     *
     * @param file MultipartFile of BYR statements
     * @return raw bytes[] for BYR statement
     * @throws IOException
     */
    public byte[] convertByrStatementToBytes(MultipartFile file) throws IOException {
        validateByrStatement(file);
        return file.getBytes();
    }

    private void validateByrStatement(MultipartFile file) throws IOException {
        if (file == null) {
            throw new RuntimeException(messagesResolver.getMessage("error.upload.no.byr.file.data"));
        }
        emptyFileContentValidation(file);
        StatementType statementType = getStatementType(IOUtils.toString(file.getBytes(), propertiesResolver.getStatementEncoding()));
        if (statementType != StatementType.BYR) {
            throw new TaxException(messagesResolver.getMessage("error.upload.not.byr.statement",file.getOriginalFilename()));
        }
    }

    public InputStream convertLedgerToInputStream(MultipartFile file) throws IOException {
        validateLedger(file);
        return file.getInputStream();
    }

    private void validateLedger(MultipartFile file) {
        if (file == null) {
            throw new RuntimeException(messagesResolver.getMessage("error.upload.no.ledger.file.data"));
        }

        emptyFileContentValidation(file);

        try (HSSFWorkbook ledger = new HSSFWorkbook(file.getInputStream())) {
            if (ledger.getSheet(propertiesResolver.getLedgerSection1Part1SheetName()) == null) {
                throw new TaxException(messagesResolver.getMessage("error.upload.ledger.no.page",propertiesResolver.getLedgerSection1Part1SheetName()));
            }
            if (ledger.getSheet(propertiesResolver.getLedgerSection1Part2SheetName()) == null) {
                throw new TaxException(messagesResolver.getMessage("error.upload.ledger.no.page",propertiesResolver.getLedgerSection1Part2SheetName()));
            }
        } catch (IOException e) {
            throw new TaxException(messagesResolver.getMessage("error.upload.not.ledger",file.getOriginalFilename()));
        }

    }

    private void emptyFileContentValidation(MultipartFile... files) {
        for (MultipartFile file : files) {
            if (file.isEmpty()) {
                throw new TaxException(messagesResolver.getMessage("error.upload.empty.file", file.getOriginalFilename()));
            }
        }
    }

    private StatementType getStatementType(String statementContent) throws IOException {
        List<String> reportLines = IOUtils.readLines(new StringReader(statementContent));
        if (reportLines.get(0).contains("Выписка по валютному счету")) {
            return StatementType.USD;
        } else if (reportLines.get(0).contains("Выписка по рублёвому счету")) {
            return StatementType.BYR;
        } else {
            return StatementType.UNKNOWN;
        }
    }

    private enum StatementType {
        BYR, USD, UNKNOWN
    }
}
