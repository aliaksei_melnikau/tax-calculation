package by.helmes.tax.calculation.utils.xls.declaration;

import by.helmes.tax.calculation.models.output.Quarter;
import by.helmes.tax.calculation.models.output.TaxDeclarationEntity;
import by.helmes.tax.calculation.services.processing.TaxDeclarationEntitiesProcessor;
import by.helmes.tax.calculation.utils.date.DateUtils;
import by.helmes.tax.calculation.utils.props.MessagesResolver;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import com.google.common.collect.Lists;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Set;

/**
 * Created by aliaksei.melnikau on 28.03.16.
 */
@Component
public class TaxDeclarationXlsUtils {

    @Autowired
    private PropertiesResolver propertiesResolver;

    @Autowired
    private MessagesResolver messagesResolver;

    @Autowired
    private TaxDeclarationEntitiesProcessor taxDeclarationEntitiesProcessor;

    @Autowired
    private DateUtils dateUtils;

    public void fillTaxReport(HSSFWorkbook taxDeclarationBook, List<TaxDeclarationEntity> taxableEntities) {
        int rowIndex = 0;
        for (List<TaxDeclarationEntity> taxDeclarationEntities: taxDeclarationEntitiesProcessor.splitTaxDeclarationEntitiesByQuarters(taxableEntities))
        {
            buildYearRow(taxDeclarationBook, rowIndex++, taxDeclarationEntities.get(0).getYear());
            buildQuarterRow(taxDeclarationBook, rowIndex++, Quarter.getQuarter(taxDeclarationEntities.get(0).getMonth()));
            buildHeaderRowForQuarter(taxDeclarationBook, rowIndex++);

            for (TaxDeclarationEntity taxDeclarationEntity : taxDeclarationEntities) {
                buildMonthValuesRow(taxDeclarationBook, rowIndex++, taxDeclarationEntity);
            }
            buildQuarterValuesAggregationRow(taxDeclarationBook, rowIndex++, taxDeclarationEntities);
            buildEmptyRow(taxDeclarationBook, rowIndex++);
            buildQuarterTaxableAmountRow(taxDeclarationBook, rowIndex++, taxDeclarationEntities);
            buildQuarterTaxationRow(taxDeclarationBook, rowIndex++, taxDeclarationEntities);
            buildEmptyRow(taxDeclarationBook, rowIndex++);
            buildEmptyRow(taxDeclarationBook, rowIndex++);
        }

        fitColumnsWidth(taxDeclarationBook);
    }

    public void buildYearRow(HSSFWorkbook workbook, int rowNumber, int year) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);

        for (int i = 0; i < getHeadersColumnsCount(); i++) {
            Cell cell = createStyledCell(row, i, CellStyle.ALIGN_CENTER, IndexedColors.GREY_25_PERCENT.getIndex());
            if (i == 0) {
                cell.setCellValue(String.valueOf(year));
            }
        }
    }

    public void buildQuarterRow(HSSFWorkbook workbook, int rowNumber, Quarter quarter) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);

        for (int i = 0; i < getHeadersColumnsCount(); i++) {
            Cell cell = createStyledCell(row, i, CellStyle.ALIGN_CENTER, IndexedColors.GREY_25_PERCENT.getIndex());
            if (i == 0) {
                cell.setCellValue(messagesResolver.getMessage("tax.declaration.quarter",quarter));
            }
        }
    }

    public void buildHeaderRowForQuarter(HSSFWorkbook workbook, int rowNumber) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);

        String[] headers = messagesResolver.getMessage("tax.declaration.headers").split(",");

        for (int i = 0; i < headers.length; i++) {
            Cell cell = createStyledCell(row, i, CellStyle.ALIGN_CENTER,null);
            cell.setCellValue(headers[i]);
        }
    }

    public void buildMonthValuesRow(HSSFWorkbook workbook, int rowNumber, TaxDeclarationEntity taxDeclarationEntity) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);

        Cell monthCell = createStyledCell(row, propertiesResolver.getTaxDeclarationMonthColumnIndex(), null, null);
        monthCell.setCellValue(dateUtils.getMonthName(taxDeclarationEntity.getMonth()));

        Cell salaryCell = createStyledCell(row, propertiesResolver.getTaxDeclarationSalaryColumnIndex(), null, null);
        salaryCell.setCellValue(taxDeclarationEntity.getTaxBase().doubleValue());

        Cell nonSaleRevenueCell = createStyledCell(row, propertiesResolver.getTaxDeclarationNonSaleRevenueColumnIndex(), null, null);
        nonSaleRevenueCell.setCellValue(taxDeclarationEntity.getNonSaleRevenue().doubleValue());

        Cell combinedIncomeCell = createStyledCell(row, propertiesResolver.getTaxDeclarationCombinedIncomeColumnIndex(), null, null);
        combinedIncomeCell.setCellValue(taxDeclarationEntity.getTaxBase().add(taxDeclarationEntity.getNonSaleRevenue()).doubleValue());
    }

    public void buildQuarterValuesAggregationRow(HSSFWorkbook workbook, int rowNumber, List<TaxDeclarationEntity> taxDeclarationEntities) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);

        BigDecimal salaryAggregatedValue = getAggregatedSalaryForQuarter(taxDeclarationEntities);
        BigDecimal nonSaleRevenueAggregatedValue = getAggregatedNonSaleRevenue(taxDeclarationEntities);

        Cell monthCell = createStyledCell(row, propertiesResolver.getTaxDeclarationMonthColumnIndex(), null, null);
        monthCell.setCellValue(messagesResolver.getMessage("tax.declaration.quarter.aggregated",Quarter.getQuarter(taxDeclarationEntities.get(0).getMonth())));

        Cell salaryCell = createStyledCell(row, propertiesResolver.getTaxDeclarationSalaryColumnIndex(), null, null);
        salaryCell.setCellValue(salaryAggregatedValue.doubleValue());

        Cell nonSaleRevenueCell = createStyledCell(row, propertiesResolver.getTaxDeclarationNonSaleRevenueColumnIndex(), null, null);
        nonSaleRevenueCell.setCellValue(nonSaleRevenueAggregatedValue.doubleValue());

        Cell combinedIncomeCell = createStyledCell(row, propertiesResolver.getTaxDeclarationCombinedIncomeColumnIndex(), null, null);
        combinedIncomeCell.setCellValue(salaryAggregatedValue.add(nonSaleRevenueAggregatedValue).doubleValue());
    }

    public void buildQuarterTaxableAmountRow(HSSFWorkbook workbook, int rowNumber, List<TaxDeclarationEntity> taxDeclarationEntities) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);
        int cellIndex = 0;

        Cell labelCell = createStyledCell(row, cellIndex++, null, IndexedColors.LIGHT_GREEN.getIndex());
        labelCell.setCellValue(messagesResolver.getMessage("tax.declaration.taxable-amount",Quarter.getQuarter(taxDeclarationEntities.get(0).getMonth())));

        Cell valueCell = createStyledCell(row, cellIndex++, null, IndexedColors.LIGHT_GREEN.getIndex());
        valueCell.setCellValue(getAggregatedSalaryForQuarter(taxDeclarationEntities).add(getAggregatedNonSaleRevenue(taxDeclarationEntities)).doubleValue());
    }

    public void buildQuarterTaxationRow(HSSFWorkbook workbook, int rowNumber, List<TaxDeclarationEntity> taxDeclarationEntities) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);
        int cellIndex = 0;

        Cell labelCell = createStyledCell(row, cellIndex++, null, IndexedColors.CORAL.getIndex());
        labelCell.setCellValue(messagesResolver.getMessage("tax.declaration.taxation",Quarter.getQuarter(taxDeclarationEntities.get(0).getMonth())));

        Cell valueCell = createStyledCell(row, cellIndex++, null, IndexedColors.CORAL.getIndex());
        valueCell.setCellValue(getAggregatedSalaryForQuarter(taxDeclarationEntities).add(getAggregatedNonSaleRevenue(taxDeclarationEntities)).multiply(BigDecimal.valueOf(propertiesResolver.getTaxRate()))
                .setScale(0, RoundingMode.HALF_UP).doubleValue());
    }

    private BigDecimal getAggregatedSalaryForQuarter(List<TaxDeclarationEntity> taxDeclarationEntities) {
        BigDecimal salaryAggregatedValue = BigDecimal.ZERO;

        for (TaxDeclarationEntity taxDeclarationEntity : taxDeclarationEntities) {
            salaryAggregatedValue = salaryAggregatedValue.add(taxDeclarationEntity.getTaxBase());
        }

        return salaryAggregatedValue;
    }

    private BigDecimal getAggregatedNonSaleRevenue(List<TaxDeclarationEntity> taxDeclarationEntities) {
        BigDecimal nonSaleRevenueAggregatedValue = BigDecimal.ZERO;

        for (TaxDeclarationEntity taxDeclarationEntity : taxDeclarationEntities) {
            nonSaleRevenueAggregatedValue = nonSaleRevenueAggregatedValue.add(taxDeclarationEntity.getNonSaleRevenue());
        }

        return nonSaleRevenueAggregatedValue;
    }

    public void buildEmptyRow(HSSFWorkbook workbook, int rowNumber) {
        Row row = workbook.getSheetAt(0).createRow(rowNumber);
        Cell cell = row.createCell(0);
        cell.setCellValue("");
    }

    public void fitColumnsWidth(HSSFWorkbook workbook) {
        Row row = workbook.getSheetAt(0).getRow(0);

        for(int colNum = 0; colNum<row.getLastCellNum();colNum++)
            workbook.getSheetAt(0).autoSizeColumn(colNum);
    }

    private Font getFont(Workbook workbook) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints(propertiesResolver.getTaxDeclarationFontSize());
        font.setFontName(propertiesResolver.getTaxDeclarationFontName());

        return font;
    }

    private Cell createStyledCell(Row row, int cellIndex, Short alignment, Short colorIndex) {
        CellStyle style = row.getSheet().getWorkbook().createCellStyle();
        if (alignment!=null) {
            style.setAlignment(alignment);
        }
        if (colorIndex != null) {
            style.setFillForegroundColor(colorIndex);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        style.setFont(getFont(row.getSheet().getWorkbook()));

        Cell cell = row.createCell(cellIndex);
        cell.setCellStyle(style);
        return cell;
    }

    private int getHeadersColumnsCount() {
        String headers = messagesResolver.getMessage("tax.declaration.headers");
        return headers.split(",").length;
    }

}
