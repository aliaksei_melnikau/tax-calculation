package by.helmes.tax.calculation.utils.xls.ledger;

import by.helmes.tax.calculation.models.output.Section1Part2Entity;
import by.helmes.tax.calculation.utils.exception.TaxException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;

/**
 * Created by aliaksei.melnikau on 21.03.16.
 */
@Component
public class LedgerSection1Part2XlsUtils extends LedgerXlsUtils {
    /**
     * Extracts existing records from excel sheet with name "Раздел I ч.II п.1".
     *
     * @param sheet - sheet from ledger book which name is "Раздел I ч.II п.1"
     * @return List of existing records for given excel sheet
     */
    public List<Section1Part2Entity> extractExistingSection1Part2Entries(HSSFSheet sheet) {
        List<Section1Part2Entity> records = new ArrayList<>();
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);

        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = null;
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            // actual data can only be after row with header numbers
            if (currentRow.getRowNum() <= indexOfRowWhichHasHeaderNumbers) {
                continue;
            }
            // we do not want extract any data from last row which accumulates data for the whole year
            if (isLastRow(currentRow)) {
                break;
            }

            Map<ColumnType, Integer> columnIndexMap = new HashMap<>();
            Cell shipmentDateCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.SHIPMENT_DATE, columnIndexMap));
            Cell receiverCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.RECEIVER, columnIndexMap));
            Cell totalForMonthAmountByrCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.AMOUNT_BYR, columnIndexMap));
            Cell currencyNameCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.CURRENCY_NAME, columnIndexMap));
            Cell totalForMonthAmountCurrencyCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.AMOUNT_CURRENCY, columnIndexMap));

            if (shipmentDateCell.getCellType() == Cell.CELL_TYPE_NUMERIC && DateUtil.isCellDateFormatted(shipmentDateCell) && totalForMonthAmountByrCell.getCellType() != Cell.CELL_TYPE_BLANK) {
                Date shipmentDate = DateUtil.getJavaDate(shipmentDateCell.getNumericCellValue());
                LocalDate shipmentLocalDate = shipmentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                BigDecimal totalForMonthAmountByr = BigDecimal.valueOf(totalForMonthAmountByrCell.getNumericCellValue());

                // Collect invoices for given month
                List<Section1Part2Entity.Invoice> invoices = new ArrayList<>();
                for (int i = getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.FIRST_MONTH_INDEX, columnIndexMap);
                     i <= getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.LAST_MONTH_INDEX, columnIndexMap); i += 2) {
                    Cell documentNameCell = currentRow.getCell(i);
                    Cell amountByrCell = currentRow.getCell(i + 1);
                    if (Cell.CELL_TYPE_STRING == documentNameCell.getCellType() && Cell.CELL_TYPE_BLANK != amountByrCell.getCellType()) {
                        Section1Part2Entity.Invoice invoice = new Section1Part2Entity.Invoice();
                        invoice.setAmountByr(BigDecimal.valueOf(amountByrCell.getNumericCellValue()));
                        invoice.setDocumentName(documentNameCell.getStringCellValue());
                        invoice.setPaymentDate(ledgerEntitiesProcessor.extractInvoiceDateFromInvoiceDocumentName(documentNameCell.getStringCellValue()));
                        invoices.add(invoice);
                    }
                }
                if (invoices.size() == 0) {
                    throw new TaxException(messagesResolver.getMessage("error.ledger.xls.section1.part2.no.invoice",shipmentLocalDate.toString()));
                }

                Section1Part2Entity section1Part2Entity = new Section1Part2Entity();
                section1Part2Entity.setShipmentDate(shipmentLocalDate);
                section1Part2Entity.setAmountByr(totalForMonthAmountByr);
                section1Part2Entity.setInvoices(invoices);
                if (Cell.CELL_TYPE_STRING == currencyNameCell.getCellType() && currencyNameCell.getStringCellValue().length() > 1) {
                    section1Part2Entity.setCurrencyName(currencyNameCell.getStringCellValue());
                }
                if (Cell.CELL_TYPE_NUMERIC == totalForMonthAmountCurrencyCell.getCellType()) {
                    section1Part2Entity.setAmountCurrency(BigDecimal.valueOf(totalForMonthAmountCurrencyCell.getNumericCellValue()));
                }
                if (Cell.CELL_TYPE_STRING == receiverCell.getCellType() && receiverCell.getStringCellValue().length() > 1) {
                    section1Part2Entity.setProductReceiver(receiverCell.getStringCellValue());
                }
                records.add(section1Part2Entity);
            }
        }
        return records;
    }

    /**
     * Clears existing records for excel sheet with name "Раздел I ч.II п.1".
     *
     * @param sheet - sheet from ledger book which name is "Раздел I ч.II п.1"
     */
    public void clearExistingRecords(HSSFSheet sheet) {
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);

        int lastNullableCellIndex = getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.LAST_MANUALLY_FILLED_COLUMN, new HashMap<>());
        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = null;
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            // do not clear anything above headers
            if (currentRow.getRowNum() <= indexOfRowWhichHasHeaderNumbers) {
                continue;
            }
            if (isLastRow(currentRow)) {
                break;
            }
            Iterator<Cell> cellIterator = currentRow.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                // do not clear cells after non-sale-revenue column, as those columns are pre-filled with default values
                if (cell.getColumnIndex() > lastNullableCellIndex) {
                    break;
                }
                cell.setCellType(Cell.CELL_TYPE_BLANK);
            }
        }
    }

    public void writeSection1Part2Entities(HSSFSheet sheet, List<Section1Part2Entity> newSection1Part2Entities) {
        List<Section1Part2Entity> existingEntities = extractExistingSection1Part2Entries(sheet);
        clearExistingRecords(sheet);
        List<Section1Part2Entity> mergedEntities = ledgerEntitiesProcessor.mergeSection1Part2Entities(existingEntities, newSection1Part2Entities);
        Collections.sort(mergedEntities);
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);
        for (Section1Part2Entity section1Part2Entity : mergedEntities) {
            writeSingleSection1Part1Entity(sheet,section1Part2Entity,++indexOfRowWhichHasHeaderNumbers);
        }
        adjustRowHeights(sheet);
    }

    private void writeSingleSection1Part1Entity(HSSFSheet sheet, Section1Part2Entity section1Part2Entity, int rowIndex) {
        Row currentRow = sheet.getRow(rowIndex);
        int indexOfRowWhichHasHeaderNumbers = getRowNumberWithHeaderNumbers(sheet);

        Map<ColumnType, Integer> columnIndexMap = new HashMap<>();
        Cell shipmentDateCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.SHIPMENT_DATE, columnIndexMap));
        Cell receiverCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.RECEIVER, columnIndexMap));
        Cell totalForMonthAmountByrCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.AMOUNT_BYR, columnIndexMap));
        Cell currencyNameCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.CURRENCY_NAME, columnIndexMap));
        Cell totalForMonthAmountCurrencyCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers), ColumnType.AMOUNT_CURRENCY, columnIndexMap));

        shipmentDateCell.setCellValue(DateUtil.getExcelDate(java.sql.Date.valueOf(section1Part2Entity.getShipmentDate())));
        receiverCell.setCellValue(section1Part2Entity.getProductReceiver());
        totalForMonthAmountByrCell.setCellValue(section1Part2Entity.getAmountByr().doubleValue());
        currencyNameCell.setCellValue(section1Part2Entity.getCurrencyName());
        totalForMonthAmountCurrencyCell.setCellValue(section1Part2Entity.getAmountCurrency().doubleValue());

        for (Section1Part2Entity.Invoice invoice : section1Part2Entity.getInvoices()) {
            Cell invoiceDocumentCell = currentRow.getCell(getColumnIndex(sheet.getRow(indexOfRowWhichHasHeaderNumbers),getInvoiceMonthHeaderValue(invoice.getPaymentDate().getMonth())));
            Cell invoiceAmountCell = currentRow.getCell(invoiceDocumentCell.getColumnIndex()+1);

            invoiceDocumentCell.setCellValue(invoice.getDocumentName());
            invoiceAmountCell.setCellValue(invoice.getAmountByr().doubleValue());

        }
    }

    private double getInvoiceMonthHeaderValue(Month invoiceMonth) {
        BigDecimal initialHeaderValue = BigDecimal.valueOf(getHeaderColumnValue(ColumnType.FIRST_MONTH_INDEX));
        for (Month month : Month.values()) {
            if (month == invoiceMonth) {
                return initialHeaderValue.doubleValue();
            }
            initialHeaderValue = initialHeaderValue.add(BigDecimal.valueOf(2));
        }
        throw new IllegalArgumentException("invoiceMonth cannot be null");
    }


    private boolean isLastRow(Row currentRow) {
        Iterator<Cell> cellIterator = currentRow.cellIterator();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if (cell.getCellType() == Cell.CELL_TYPE_STRING && propertiesResolver.getLedgerSection1Part2LastRowContent().equals(cell.getStringCellValue())) {
                return true;
            }
        }
        return false;
    }

}
