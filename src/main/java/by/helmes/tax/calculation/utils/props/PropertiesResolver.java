package by.helmes.tax.calculation.utils.props;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by melnikau on 1/29/16.
 */
@Component
public class PropertiesResolver {

    @Value("${multipart.maxFileSize}")
    private String fileSizeLimit;

    @Value("${number.of.currency.statements}")
    private int numberOfCurrencyStatements;

    @Value("${statement.encoding}")
    private String statementEncoding;

    @Value("${response.error.header}")
    private String responseErrorHeader;

    @Value("${ledger.book.dateformat}")
    private String ledgerDateFormat;

    @Value("${ledger.book.filename}")
    private String ledgerFilename;

    @Value("${ledger.book.section1.part1.name}")
    private String ledgerSection1Part1SheetName;

    @Value("${ledger.book.section1.part1.section.quarter.name}")
    private String ledgerSection1Part1QuarterHeader;

    @Value("${ledger.book.section1.part1.section.year.name}")
    private String ledgerSection1Part1YearHeader;

    @Value("${ledger.book.section1.part1.header.date.column.name}")
    private Double ledgerSection1Part1DateColumnName;

    @Value("${ledger.book.section1.part1.header.document.column.name}")
    private Double ledgerSection1Part1DocumentColumnName;

    @Value("${ledger.book.section1.part1.header.operation.column.name}")
    private Double ledgerSection1Part1OperationColumnName;

    @Value("${ledger.book.section1.part1.header.total.column.name}")
    private Double ledgerSection1Part1RevenueColumnName;

    @Value("${ledger.book.section1.part1.header.non-revenue.column.name}")
    private Double ledgerSection1Part1NonRevenueColumnName;

    @Value("${ledger.book.section1.part2.name}")
    private String ledgerSection1Part2SheetName;

    @Value("${ledger.book.section1.part2.header.shipment-date.column.name}")
    private Double ledgerSection1Part2ShipmentDateColumnName;

    @Value("${ledger.book.section1.part2.header.receiver.column.name}")
    private Double ledgerSection1Part2ReceiverColumnName;

    @Value("${ledger.book.section1.part2.header.total-shipment-cost.column.name}")
    private Double ledgerSection1Part2ShipmentCostForMonthByrColumnName;

    @Value("${ledger.book.section1.part2.header.currency-name.column.name}")
    private Double ledgerSection1Part2ShipmentCurrencyColumnName;

    @Value("${ledger.book.section1.part2.header.total-shipment-cost-currency.column.name}")
    private Double ledgerSection1Part2ShipmentCostForMonthCurrencyColumnName;

    @Value("${ledger.book.section1.part2.header.first-month.column.name}")
    private Double ledgerSection1Part2FirstMonthColumnName;

    @Value("${ledger.book.section1.part2.header.last-month.column.name}")
    private Double ledgerSection1Part2LastMonthColumnName;

    @Value("${ledger.book.section1.part2.header.last-manually-filled.column.name}")
    private Double ledgerSection1Part2LastManuallyFilledColumnName;

    @Value("${ledger.book.section1.part2.last-row.name}")
    private String ledgerSection1Part2LastRowContent;

    @Value("${payment.processing.invoice.helmes.keyword}")
    private String paymentProcessingHelmesKeyword;

    @Value("${payment.processing.invoice.invoice.keyword}")
    private String paymentProcessingInvoiceKeyword;

    @Value("${payment.processing.invoice.according.keyword}")
    private String paymentProcessingAccordingKeyword;

    @Value("${payment.processing.invoice.nr.keyword}")
    private String paymentProcessingNrKeyword;

    @Value("${payment.processing.keyword.nazn-non-obligatory-currency-selling}")
    private String naznNonObligatoryCurrencySellingKeyword;

    @Value("${payment.processing.keyword.nazn-obligatory-currency-selling-usd}")
    private String naznObligatoryCurrencySellingKeywordUsd;

    @Value("${payment.processing.keyword.nazn-obligatory-currency-selling-byr}")
    private String naznObligatoryCurrencySellingKeywordByr;

    @Value("${payment.processing.keyword.kor-name-non-obligatory-currency-selling}")
    private String korNameNonObligatoryCurrencySellingKeyword;

    @Value("${payment.processing.keyword.kor-name-obligatory-currency-selling}")
    private String korNameObligatoryCurrencySellingKeyword;

    @Value("${payment.processing.keyword.fee}")
    private String paymentProcessingByrFeeKeyword;

    @Value("${tax.declaration.filename}")
    private String taxDeclarationFilename;

    @Value("${tax.declaration.font-name}")
    private String taxDeclarationFontName;

    @Value("${tax.declaration.font-size}")
    private Short taxDeclarationFontSize;

    @Value("${tax.declaration.month.column.index}")
    private Integer taxDeclarationMonthColumnIndex;

    @Value("${tax.declaration.salary.column.index}")
    private Integer taxDeclarationSalaryColumnIndex;

    @Value("${tax.declaration.non-sale-revenue.column.index}")
    private Integer taxDeclarationNonSaleRevenueColumnIndex;

    @Value("${tax.declaration.combined-income.column.index}")
    private Integer taxDeclarationCombinedIncomeColumnIndex;

    @Value("${tax.declaration.tax.rate}")
    private Double taxRate;

    @Value("${parsing.obligatory.currency.selling.fee.percent}")
    private Double obligatorySellingBankFeePercent;

    public String getFileSizeLimit() {
        return fileSizeLimit;
    }

    public int getNumberOfCurrencyStatements() {
        return numberOfCurrencyStatements;
    }

    public String getStatementEncoding() {
        return statementEncoding;
    }

    public String getResponseErrorHeader() {
        return responseErrorHeader;
    }

    public String getLedgerDateFormat() {
        return ledgerDateFormat;
    }

    public String getLedgerFilename() {
        return ledgerFilename;
    }

    public String getLedgerSection1Part1SheetName() {
        return ledgerSection1Part1SheetName;
    }

    public String getLedgerSection1Part2SheetName() {
        return ledgerSection1Part2SheetName;
    }

    public String getLedgerSection1Part1QuarterHeader() {
        return ledgerSection1Part1QuarterHeader;
    }

    public String getLedgerSection1Part1YearHeader() {
        return ledgerSection1Part1YearHeader;
    }

    public Double getLedgerSection1Part1DateColumnName() {
        return ledgerSection1Part1DateColumnName;
    }

    public Double getLedgerSection1Part1DocumentColumnName() {
        return ledgerSection1Part1DocumentColumnName;
    }

    public Double getLedgerSection1Part1OperationColumnName() {
        return ledgerSection1Part1OperationColumnName;
    }

    public Double getLedgerSection1Part1RevenueColumnName() {
        return ledgerSection1Part1RevenueColumnName;
    }

    public Double getLedgerSection1Part1NonRevenueColumnName() {
        return ledgerSection1Part1NonRevenueColumnName;
    }

    public Double getLedgerSection1Part2ShipmentDateColumnName() {
        return ledgerSection1Part2ShipmentDateColumnName;
    }

    public Double getLedgerSection1Part2ReceiverColumnName() {
        return ledgerSection1Part2ReceiverColumnName;
    }

    public Double getLedgerSection1Part2ShipmentCostForMonthByrColumnName() {
        return ledgerSection1Part2ShipmentCostForMonthByrColumnName;
    }

    public Double getLedgerSection1Part2ShipmentCurrencyColumnName() {
        return ledgerSection1Part2ShipmentCurrencyColumnName;
    }

    public Double getLedgerSection1Part2ShipmentCostForMonthCurrencyColumnName() {
        return ledgerSection1Part2ShipmentCostForMonthCurrencyColumnName;
    }

    public Double getLedgerSection1Part2FirstMonthColumnName() {
        return ledgerSection1Part2FirstMonthColumnName;
    }

    public Double getLedgerSection1Part2LastMonthColumnName() {
        return ledgerSection1Part2LastMonthColumnName;
    }

    public Double getLedgerSection1Part2LastManuallyFilledColumnName() {
        return ledgerSection1Part2LastManuallyFilledColumnName;
    }

    public String getLedgerSection1Part2LastRowContent() {
        return ledgerSection1Part2LastRowContent;
    }

    public String getPaymentProcessingHelmesKeyword() {
        return paymentProcessingHelmesKeyword;
    }

    public String getPaymentProcessingInvoiceKeyword() {
        return paymentProcessingInvoiceKeyword;
    }

    public String getPaymentProcessingAccordingKeyword() {
        return paymentProcessingAccordingKeyword;
    }

    public String getPaymentProcessingNrKeyword() {
        return paymentProcessingNrKeyword;
    }

    public String getNaznNonObligatoryCurrencySellingKeyword() {
        return naznNonObligatoryCurrencySellingKeyword;
    }

    public String getNaznObligatoryCurrencySellingKeywordUsd() {
        return naznObligatoryCurrencySellingKeywordUsd;
    }

    public String getNaznObligatoryCurrencySellingKeywordByr() {
        return naznObligatoryCurrencySellingKeywordByr;
    }

    public String getKorNameNonObligatoryCurrencySellingKeyword() {
        return korNameNonObligatoryCurrencySellingKeyword;
    }

    public String getKorNameObligatoryCurrencySellingKeyword() {
        return korNameObligatoryCurrencySellingKeyword;
    }

    public String getPaymentProcessingByrFeeKeyword() {
        return paymentProcessingByrFeeKeyword;
    }

    public String getTaxDeclarationFilename() {
        return taxDeclarationFilename;
    }

    public String getTaxDeclarationFontName() {
        return taxDeclarationFontName;
    }

    public Short getTaxDeclarationFontSize() {
        return taxDeclarationFontSize;
    }

    public Integer getTaxDeclarationMonthColumnIndex() {
        return taxDeclarationMonthColumnIndex;
    }

    public Integer getTaxDeclarationSalaryColumnIndex() {
        return taxDeclarationSalaryColumnIndex;
    }

    public Integer getTaxDeclarationNonSaleRevenueColumnIndex() {
        return taxDeclarationNonSaleRevenueColumnIndex;
    }

    public Integer getTaxDeclarationCombinedIncomeColumnIndex() {
        return taxDeclarationCombinedIncomeColumnIndex;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public Double getObligatorySellingBankFeePercent() {
        return obligatorySellingBankFeePercent;
    }
}
