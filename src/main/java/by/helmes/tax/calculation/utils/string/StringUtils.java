package by.helmes.tax.calculation.utils.string;

import org.springframework.stereotype.Component;

/**
 * @author Aliaksei Melnikau
 */
@Component
public class StringUtils {
    public String clearNonLetterOrDigitsFromWordEdges(String word, boolean removeFromStart) {
        StringBuilder value = new StringBuilder(word);
        StringBuilder result;
        if (!removeFromStart) {
            value.reverse();
        }
        int offset = 0;
        for (int i = 0; i < value.length(); i++) {
            char ch = value.charAt(i);
            if (Character.isLetterOrDigit(ch)) {
                break;
            }
            offset++;
        }
        result = new StringBuilder(value.toString().substring(offset));
        if (!removeFromStart) {
            return result.reverse().toString();
        }

        return result.toString();
    }
}
