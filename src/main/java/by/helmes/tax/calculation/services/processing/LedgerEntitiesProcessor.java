package by.helmes.tax.calculation.services.processing;

import by.helmes.tax.calculation.models.output.Quarter;
import by.helmes.tax.calculation.models.output.Section1Part1Entity;
import by.helmes.tax.calculation.models.output.Section1Part2Entity;
import by.helmes.tax.calculation.utils.date.DateUtils;
import by.helmes.tax.calculation.utils.exception.TaxException;
import by.helmes.tax.calculation.utils.props.MessagesResolver;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aliaksei.melnikau on 22.03.16.
 */
@Component
public class LedgerEntitiesProcessor {

    @Autowired
    private MessagesResolver messagesResolver;

    @Autowired
    private PropertiesResolver propertiesResolver;

    @Autowired
    private DateUtils dateUtils;

    public List<Section1Part2Entity> toSection1Part2Entities(List<Section1Part1Entity> section1Part1Entities) {
        List<Section1Part2Entity> section1Part2Entities = new ArrayList<>();
        for (Section1Part1Entity section1Part1Entity : section1Part1Entities) {
            // non-sale revenues are not included in section 1 part 2
            if (section1Part1Entity.getType() == Section1Part1Entity.RevenueType.SALARY) {
                Section1Part2Entity section1Part2Entity = new Section1Part2Entity();
                section1Part2Entity.setShipmentDate(dateUtils.getLastDayOfPreviousMonth(section1Part1Entity.getDate()));
                section1Part2Entity.setProductReceiver(messagesResolver.getMessage("ledger.book.section1.part2.receiver.name"));
                section1Part2Entity.setAmountByr(section1Part1Entity.getAmountByr());
                if (section1Part1Entity.getAmountCurrency() != null) {
                    section1Part2Entity.setAmountCurrency(section1Part1Entity.getAmountCurrency());
                    section1Part2Entity.setCurrencyName("USD"); // TODO think about support for other currencies
                }

                Section1Part2Entity.Invoice invoice = new Section1Part2Entity.Invoice();
                invoice.setPaymentDate(section1Part1Entity.getDate());
                invoice.setAmountByr(section1Part1Entity.getAmountByr());
                invoice.setDocumentName(extractInvoiceInformationFromSection1Part1Entity(section1Part1Entity));

                section1Part2Entity.setInvoices(Arrays.asList(invoice));
                section1Part2Entities.add(section1Part2Entity);
            }
        }

        return section1Part2Entities;
    }

    private String extractInvoiceInformationFromSection1Part1Entity(Section1Part1Entity section1Part1Entity) {
        int commaIndex = section1Part1Entity.getDocumentName().lastIndexOf(",");
        if (section1Part1Entity.getType() == Section1Part1Entity.RevenueType.SALARY && commaIndex > -1) {
            return section1Part1Entity.getDocumentName().substring(commaIndex+1).trim();
        }
        throw new TaxException(messagesResolver.getMessage("error.ledger.section1.part1.extract.invoice",section1Part1Entity.getDocumentName()));
    }

    public LocalDate extractInvoiceDateFromInvoiceDocumentName(String documentName) {
        int dateLength = propertiesResolver.getLedgerDateFormat().length();
        String dateValue = documentName.trim().substring(documentName.length()-dateLength);
        return dateUtils.parseDate(dateValue);
    }

    public List<Section1Part2Entity> mergeSection1Part2Entities(List<Section1Part2Entity> existingEntities, List<Section1Part2Entity> newEntities) {
        List<Section1Part2Entity> mergedList = new ArrayList<>(existingEntities);
        for (Section1Part2Entity section1Part2Entity : newEntities) {
            if (!mergedList.contains(section1Part2Entity)) {
                mergedList.add(section1Part2Entity);
            } else {
                throw new TaxException(messagesResolver.getMessage("error.ledger.section1.part2.merge.duplicate",section1Part2Entity.getShipmentDate()));
            }
        }
        return mergedList;
    }

    public List<Section1Part1Entity> mergeSection1Part1Entities(List<Section1Part1Entity> existingEntities, List<Section1Part1Entity> newEntities) {
        List<Section1Part1Entity> mergedList = new ArrayList<>(existingEntities);
        for (Section1Part1Entity section1Part1Entity : newEntities) {
            if (!mergedList.contains(section1Part1Entity)) {
                mergedList.add(section1Part1Entity);
            } else {
                throw new TaxException(messagesResolver.getMessage("error.ledger.section1.part1.merge.duplicate",section1Part1Entity.getDate(),section1Part1Entity.getAmountByr().toBigInteger().toString()));
            }
        }
        return mergedList;
    }

    public List<Section1Part1Entity> getSection1Part1EntitiesForQuarter(List<Section1Part1Entity> entities, Quarter quarter) {
        List<Section1Part1Entity> filteredList = new ArrayList<>();
        for (Section1Part1Entity section1Part1Entity : entities) {
            if (quarter.getMatchingMonths().contains(section1Part1Entity.getDate().getMonth())) {
                filteredList.add(section1Part1Entity);
            }
        }
        return filteredList;
    }
}
