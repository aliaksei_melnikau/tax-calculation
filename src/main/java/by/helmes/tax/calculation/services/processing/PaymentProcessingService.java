package by.helmes.tax.calculation.services.processing;

import by.helmes.tax.calculation.models.payment.ByrPayment;
import by.helmes.tax.calculation.models.payment.UsdPayment;
import by.helmes.tax.calculation.models.output.Section1Part1Entity;
import by.helmes.tax.calculation.models.output.TaxDeclarationEntity;
import by.helmes.tax.calculation.models.output.Section1Part1Entity.RevenueType;
import by.helmes.tax.calculation.services.currencies.NationalBankExchangeRatesService;
import by.helmes.tax.calculation.utils.date.DateUtils;
import by.helmes.tax.calculation.utils.exception.TaxException;
import by.helmes.tax.calculation.utils.math.MathUtils;
import by.helmes.tax.calculation.utils.props.MessagesResolver;

import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import by.helmes.tax.calculation.utils.string.StringUtils;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

/**
 * Created by melnikau on 1/29/16.
 */
@Service
public class PaymentProcessingService {

    @Autowired
    private MessagesResolver messagesResolver;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private PropertiesResolver propertiesResolver;

    @Autowired
    private MathUtils mathUtils;

    @Autowired
    private StringUtils stringUtils;

    @Autowired
    private PaymentProcessingPredicates paymentProcessingPredicates;

    @Autowired
    private NationalBankExchangeRatesService nationalBankExchangeRatesService;

    public static final int TRANSFER_FEE_USD = 11;

    public List<TaxDeclarationEntity> getTaxableEntitiesForReport(List<ByrPayment> byrPayments, List<UsdPayment> usdPayments) {

        Set<TaxDeclarationEntity> reports = new HashSet<>();
        List<Section1Part1Entity> section1Part1Entities = getTaxableEntitiesForLedger(byrPayments, usdPayments);
        for(Section1Part1Entity section1Part1Entity : section1Part1Entities){
        	if(section1Part1Entity.getType() == RevenueType.SALARY){
        		addSalaryToReports(reports, section1Part1Entity);
        	}else if(section1Part1Entity.getType() == RevenueType.NON_SALE){
        		addNonSaleRevenueToReports(reports, section1Part1Entity);
        	}
        }

        return Lists.newArrayList(reports);
    }

    public Set<UsdPayment> getSalaryPayments(List<UsdPayment> allUsdPayments) {
        return new HashSet<>(Collections2.filter(allUsdPayments, paymentProcessingPredicates.getSalaryOperationsPredicateUsd()));
    }

    private Set<UsdPayment> getAllPotentialNonSaleRevenuesOperationsUsd(List<UsdPayment> allUsdPayments) {
        return new HashSet<>(Collections2.filter(allUsdPayments,
                Predicates.or(paymentProcessingPredicates.getNonObligatoryPotentialNonSaleRevenuesOperationsPredicateUsd(),
                paymentProcessingPredicates.getObligatoryPotentialNonSaleRevenuesOperationsPredicateUsd())));
    }

    private Set<ByrPayment> getAllPotentialNonSaleRevenuesOperationsByr(List<ByrPayment> allByrPayments) {
        return new HashSet<>(Collections2.filter(allByrPayments,
                Predicates.or(paymentProcessingPredicates.getNonObligatoryPotentialNonSaleRevenuesOperationsPredicateByr(),
                        paymentProcessingPredicates.getObligatoryPotentialNonSaleRevenuesOperationsPredicateByr())));
    }

    private Set<ByrPayment> getObligatoryPotentialNonSaleRevenuesOperationsByr(List<ByrPayment> allByrPayments) {
      return new HashSet<>(Collections2.filter(allByrPayments,
                      paymentProcessingPredicates.getObligatoryPotentialNonSaleRevenuesOperationsPredicateByr()));
    }

    private Set<UsdPayment> getObligatoryPotentialNonSaleRevenuesOperationsUsd(List<UsdPayment> allUsdPayments) {
      return new HashSet<>(Collections2.filter(allUsdPayments,
              paymentProcessingPredicates.getObligatoryPotentialNonSaleRevenuesOperationsPredicateUsd()));
    }

    public List<Section1Part1Entity> getTaxableEntitiesForLedger(List<ByrPayment> byrPayments, List<UsdPayment> usdPayments) {
        if (byrPayments == null || byrPayments.size() == 0) {
            throw new TaxException("error.payment.processing.no.byr.payments");
        }
        if (usdPayments==null || usdPayments.size() == 0) {
            throw new TaxException("error.payment.processing.no.currency.payments");
        }

        List<Section1Part1Entity> taxableEntities = new ArrayList<>();

        Set<UsdPayment> allPotentialNonSaleRevenuesOperationsUsd = getAllPotentialNonSaleRevenuesOperationsUsd(usdPayments);

        Set<ByrPayment> allPotentialNonSaleRevenuesOperationsByr = getAllPotentialNonSaleRevenuesOperationsByr(byrPayments);

        Set<UsdPayment> salaryPayments = getSalaryPayments(usdPayments);

        if (allPotentialNonSaleRevenuesOperationsByr.size() != allPotentialNonSaleRevenuesOperationsUsd.size()) {
            throw new TaxException(messagesResolver.getMessage("error.payment.processing.operations.mismatch"));
        }

        correctByrAmountForObligatoryCurrencyOperations(byrPayments,usdPayments);

        for (UsdPayment usdPayment : salaryPayments) {
            addTransferFee(usdPayment);

            Section1Part1Entity section1Part1Entity = new Section1Part1Entity(RevenueType.SALARY, usdPayment.getDocDate(), usdPayment.getCre());
            section1Part1Entity.setOperationName(messagesResolver.getMessage("ledger.book.section1.part1.salary.operation.name"));
            section1Part1Entity.setDocumentName(extractDocumentNameFromSalaryPaymentForSection1Part1(usdPayment));
            section1Part1Entity.setAmountCurrency(usdPayment.getCreQ());
            taxableEntities.add(section1Part1Entity);
        }

        for (ByrPayment byrPayment : allPotentialNonSaleRevenuesOperationsByr) {
            boolean matchFound = false;
            if (byrPayment.getDocDate() == null) {
                throw new TaxException(messagesResolver.getMessage("error.payment.processing.empty.date.byr", byrPayment.getDocSourceId().toString()));
            }
            for (UsdPayment usdPayment : allPotentialNonSaleRevenuesOperationsUsd) {
                if (usdPayment.getInDocId().equals(byrPayment.getDocSourceId())) {
                    matchFound = true;
                    BigDecimal nonSaleRevenueValue = byrPayment.getCredit().subtract(usdPayment.getDeb());
                    if (nonSaleRevenueValue.compareTo(BigDecimal.ZERO) > 0) {
                        Section1Part1Entity section1Part1Entity = new Section1Part1Entity(RevenueType.NON_SALE, byrPayment.getDocDate(), nonSaleRevenueValue);
                        section1Part1Entity.setOperationName(messagesResolver.getMessage("ledger.book.section1.part1.non-sale-revenue.operation.name"));
                        section1Part1Entity.setDocumentName("-");
                        taxableEntities.add(section1Part1Entity);
                    }
                    break;
                }
            }
            if (!matchFound) {
                throw new TaxException(messagesResolver.getMessage("error.payment.processing.no.matching.currency.operation", byrPayment.getDocSourceId().toString()));
            }
        }


        return taxableEntities;
    }

  // TODO make configurable
  private void addTransferFee(UsdPayment usdPayment) {
    try {
      BigDecimal feeAmount = new BigDecimal(TRANSFER_FEE_USD);
      usdPayment.setCreQ(usdPayment.getCreQ().add(feeAmount));
      usdPayment.setCre(nationalBankExchangeRatesService.getExchangeRate(usdPayment.getDocDate()).multiply(usdPayment.getCreQ()));
      if (usdPayment.getDocDate() == null) {
        throw new TaxException(messagesResolver.getMessage("error.payment.processing.empty.date.currency", usdPayment.getInDocId().toString()));
      }
    } catch (Exception e) {
      throw new TaxException(messagesResolver.getMessage("error.payment.processing.national.bank.api.error", e.getMessage()));
    }
  }

  private void addSalaryToReports(Set<TaxDeclarationEntity> reports, Section1Part1Entity section1Part1Entity) {
		TaxDeclarationEntity taxDeclarationEntity = new TaxDeclarationEntity(section1Part1Entity.getDate().getYear(), section1Part1Entity.getDate().getMonth());
		taxDeclarationEntity.setTaxBase(section1Part1Entity.getAmountByr());
		mergeToReports(reports, taxDeclarationEntity);
	}

	private void addNonSaleRevenueToReports(Set<TaxDeclarationEntity> reports, Section1Part1Entity section1Part1Entity) {
		TaxDeclarationEntity taxDeclarationEntity = new TaxDeclarationEntity(section1Part1Entity.getDate().getYear(), section1Part1Entity.getDate().getMonth());
		taxDeclarationEntity.setNonSaleRevenue(section1Part1Entity.getAmountByr());
		mergeToReports(reports, taxDeclarationEntity);
	}

    private void mergeToReports(Set<TaxDeclarationEntity> reports, TaxDeclarationEntity newReport) {
        boolean matchFound = false;
        for (TaxDeclarationEntity report : reports) {
            if (report.equals(newReport)) {
                report.setNonSaleRevenue(report.getNonSaleRevenue().add(newReport.getNonSaleRevenue()));
                report.setTaxBase(report.getTaxBase().add(newReport.getTaxBase()));
                matchFound = true;
                break;
            }
        }
        if (!matchFound) {
            reports.add(newReport);
        }
    }

    String extractInvoiceNumberFromSalaryPayment(UsdPayment payment) {
        String invoiceNumber = null;
        String nr = propertiesResolver.getPaymentProcessingNrKeyword();
        if (payment.getNazn() == null || payment.getNazn().isEmpty()) {
            throw new TaxException("error.payment.processing.no.nazn.field");
        }
        if (payment.getNazn().toUpperCase().indexOf(nr) == payment.getNazn().toUpperCase().lastIndexOf(nr)
                && payment.getNazn().toUpperCase().indexOf(nr) >= 0) {
            String accordingLikeWord = mathUtils.findSimilarWord(propertiesResolver.getPaymentProcessingAccordingKeyword(),
                    payment.getNazn().substring(payment.getNazn().toUpperCase().indexOf(nr)).split("\\s+"));
            if (accordingLikeWord != null) {
                invoiceNumber = payment.getNazn().substring(payment.getNazn().toUpperCase().indexOf(nr) + nr.length(), payment.getNazn().indexOf(accordingLikeWord));
                invoiceNumber = stringUtils.clearNonLetterOrDigitsFromWordEdges(invoiceNumber, true);
                invoiceNumber = stringUtils.clearNonLetterOrDigitsFromWordEdges(invoiceNumber, false);
            }
        }
        if (invoiceNumber == null || invoiceNumber.length() == 0) {
            return messagesResolver.getMessage("payment.processing.unknown.invoice.number");
        }

        return invoiceNumber;
    }

    private String extractDocumentNameFromSalaryPaymentForSection1Part1(UsdPayment usdPayment) {
        String invoiceNumber = extractInvoiceNumberFromSalaryPayment(usdPayment);
        LocalDate shipmentDate = dateUtils.getLastDayOfPreviousMonth(usdPayment.getDocDate());
        return messagesResolver.getMessage("ledger.book.section1.part1.salary.document.name",
                dateUtils.getDateFormatter().format(shipmentDate), invoiceNumber, dateUtils.getDateFormatter().format(usdPayment.getDocDate()));
    }

  /**
   * When currency is sold on currency market we might need to adjust byr/byn equivalent for that operation. We need to do this
   * when request to sell currency is issued on one day, but actual operation takes place on the next day.
   *
   */
  private void correctByrAmountForObligatoryCurrencyOperations(List<ByrPayment> byrPayments, List<UsdPayment> usdPayments) {
    Set<UsdPayment> obligatoryPotentialNonSaleRevenuesOperationsUsd = getObligatoryPotentialNonSaleRevenuesOperationsUsd(usdPayments);

    Set<ByrPayment> obligatoryPotentialNonSaleRevenuesOperationsByr = getObligatoryPotentialNonSaleRevenuesOperationsByr(byrPayments);

    for (ByrPayment byrPayment : obligatoryPotentialNonSaleRevenuesOperationsByr) {
      boolean matchFound = false;
      if (byrPayment.getDocDate() == null) {
        throw new TaxException(messagesResolver.getMessage("error.payment.processing.empty.date.byr", byrPayment.getDocSourceId().toString()));
      }
      for (UsdPayment usdPayment : obligatoryPotentialNonSaleRevenuesOperationsUsd) {
        if (usdPayment.getDocDate() == null) {
          throw new TaxException(messagesResolver.getMessage("error.payment.processing.empty.date.usd", usdPayment.getInDocId().toString()));
        }
        if (usdPayment.getInDocId().equals(byrPayment.getDocSourceId())) {
          matchFound = true;
          // different operation dates - we need to correct equivalent byr amount
          if (!usdPayment.getDocDate().isEqual(byrPayment.getDocDate())) {
            BigDecimal originalDeb = usdPayment.getDeb();
            BigDecimal exchangeRate = null;
            try {
              exchangeRate = nationalBankExchangeRatesService.getExchangeRate(byrPayment.getDocDate());
            } catch (Exception e) {
              throw new TaxException(messagesResolver.getMessage("error.payment.processing.national.bank.api.error", e.getMessage()));
            }
            usdPayment.setDeb(exchangeRate.multiply(usdPayment.getDebQ()));
            System.out.println(String.format("Doc Id: %s, Usd date: %s, Byr date: %s, Original amount: %s, New amount: %s",
                    usdPayment.getInDocId(), usdPayment.getDocDate(), byrPayment.getDocDate(), originalDeb, usdPayment.getDeb()));
          }
          break;
        }
      }
      if (!matchFound) {
        throw new TaxException(messagesResolver.getMessage("error.payment.processing.no.matching.currency.operation", byrPayment.getDocSourceId().toString()));
      }
    }
  }

}
