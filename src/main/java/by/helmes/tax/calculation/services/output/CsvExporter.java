package by.helmes.tax.calculation.services.output;

import by.helmes.tax.calculation.models.output.TaxDeclarationEntity;
import by.helmes.tax.calculation.utils.date.DateUtils;
import by.helmes.tax.calculation.utils.props.MessagesResolver;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.Month;
import java.util.*;

/**
 * Created by melnikau on 1/29/16.
 */
@Component
@Deprecated
public class CsvExporter {

    @Autowired
    private MessagesResolver messagesResolver;

    @Autowired
    private DateUtils dateUtils;

    public String generateCsvReport(List<TaxDeclarationEntity> reports) throws IOException {
        Collections.sort(reports);

        StringWriter writer = new StringWriter();
        try (CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.EXCEL.withDelimiter(','))) {
            csvPrinter.printRecord(getHeader());
            for (TaxDeclarationEntity monthReport : reports) {
                csvPrinter.printRecord(getRow(monthReport));
            }
            csvPrinter.flush();
        }
        return writer.toString();
    }



    private List<String> getHeader() {
        return Arrays.asList(messagesResolver.getMessage("report.csv.headers").split(","));
    }

    private List<String> getRow(TaxDeclarationEntity monthReport) {
        return Arrays.asList(String.valueOf(monthReport.getYear()), dateUtils.getMonthName(monthReport.getMonth()),
                formatBigDecimal(monthReport.getTaxBase()), formatBigDecimal(monthReport.getNonSaleRevenue()));
    }

    private String formatBigDecimal(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            return "0";
        }
        return bigDecimal.setScale(0).toPlainString();
    }

}
