package by.helmes.tax.calculation.services.currencies;

import by.helmes.tax.calculation.models.currencies.CurrencyExchangeRate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Aliaksei Melnikau (extmua@audatex.com)
 */
@Service
public class NationalBankExchangeRatesService {

  public static final String NATIONAL_BANK_EXCHANGE_API_ENDPOINT = "http://www.nbrb.by/API/ExRates/Rates/{0}?onDate={1}";
  public static final String API_USD_CURRENCY_ID = "145";
  public static final String API_DATE_FORMAT = "yyyy-MM-dd";

  /**
   * Returns exchange rate of one unit of currency to Belorussian Ruble
   *
   * @param date
   * @param currencyId
   * @return exchange rate to Belorussian Ruble
   * @throws org.springframework.web.client.RestClientException
   */
  public BigDecimal getExchangeRate(LocalDate date, String currencyId) {
    RestTemplate restTemplate = new RestTemplate();

    CurrencyExchangeRate currencyExchangeRate = restTemplate.getForObject(
            MessageFormat.format(NATIONAL_BANK_EXCHANGE_API_ENDPOINT, currencyId, date.format(DateTimeFormatter.ofPattern(API_DATE_FORMAT))),
            CurrencyExchangeRate.class);

    return currencyExchangeRate.getRateToByn();
  }

  /**
   *
   * Returns exchange rate of one American dollar to Belorussian Ruble
   *
   * @param date
   * @return exchange rate to Belorussian Ruble
   * @throws org.springframework.web.client.RestClientException
   */
  public BigDecimal getExchangeRate(LocalDate date) {
    return this.getExchangeRate(date,API_USD_CURRENCY_ID);
  }
}
