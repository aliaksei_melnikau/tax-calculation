package by.helmes.tax.calculation.services.processing;

import by.helmes.tax.calculation.models.payment.ByrPayment;
import by.helmes.tax.calculation.models.payment.UsdPayment;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import com.google.common.base.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Aliaksei Melnikau (extmua@audatex.com)
 */
@Component
public class PaymentProcessingPredicates {

  @Autowired
  private PropertiesResolver propertiesResolver;

  /**
   * Returns predicate that matches salary payments coming to USD transit account
   *
   * @return
   */
  public Predicate<UsdPayment> getSalaryOperationsPredicateUsd() {
    return payment -> {
      if (payment.getNazn()!=null && payment.getNazn().contains(propertiesResolver.getPaymentProcessingHelmesKeyword())
              && payment.getNazn().contains(propertiesResolver.getPaymentProcessingInvoiceKeyword())) {
        return true;
      }
      return false;
    };
  }

  /**
   * Returns predicate that matches operations of <b>obligatory</b> currency selling on currency market (30% of income)
   * for <b>currency</b> account
   *
   * @return predicate for matching operations
   */
  public Predicate<UsdPayment> getObligatoryPotentialNonSaleRevenuesOperationsPredicateUsd() {
    return payment -> {
      if (payment.getKorName()!=null &&
              payment.getKorName().toUpperCase().contains(propertiesResolver.getKorNameObligatoryCurrencySellingKeyword().toUpperCase())) {
        return true;
      }
      if (payment.getNazn()!=null && (payment.getNazn().contains(propertiesResolver.getNaznObligatoryCurrencySellingKeywordUsd()))) {
        return true;
      }
      return false;
    };
  }

  /**
   * Returns predicate that matches operations of <b>obligatory</b> currency selling on currency market (30% of income)
   * coming to <b>BYR/BYN</b> account
   *
   * @return predicate for matching operations
   */
  public Predicate<ByrPayment> getObligatoryPotentialNonSaleRevenuesOperationsPredicateByr() {
    return payment -> {
      if (payment.getKorName()!=null &&
              payment.getKorName().toUpperCase().contains(propertiesResolver.getKorNameObligatoryCurrencySellingKeyword().toUpperCase()))  {
        return true;
      }
      if (payment.getNazn()!=null && (payment.getNazn().contains(propertiesResolver.getNaznObligatoryCurrencySellingKeywordByr()))) {
        return true;
      }
      return false;
    };
  }

  /**
   * Returns predicate that matches operations of <b>non-obligatory</b> currency selling on off-exchange market (70% of income)
   * for <b>currency</b> account
   *
   * @return predicate for matching operations
   */
  public Predicate<UsdPayment> getNonObligatoryPotentialNonSaleRevenuesOperationsPredicateUsd() {
    return payment -> {
      if (payment.getKorName()!=null &&
              payment.getKorName().toUpperCase().contains(propertiesResolver.getKorNameNonObligatoryCurrencySellingKeyword().toUpperCase())) {
        return true;
      }
      if (payment.getNazn()!=null && (payment.getNazn().contains(propertiesResolver.getNaznNonObligatoryCurrencySellingKeyword()))) {
        return true;
      }
      return false;
    };
  }

  /**
   * Returns predicate that matches operations of <b>non-obligatory</b> currency selling on off-exchange market (70% of income)
   * coming to <b>BYR/BYN</b> account
   *
   * @return predicate for matching operations
   */
  public Predicate<ByrPayment> getNonObligatoryPotentialNonSaleRevenuesOperationsPredicateByr() {
    return payment -> {
      if (payment.getKorName()!=null &&
              payment.getKorName().toUpperCase().contains(propertiesResolver.getKorNameNonObligatoryCurrencySellingKeyword().toUpperCase())) {
        return true;
      }
      if (payment.getNazn()!=null && (payment.getNazn().contains(propertiesResolver.getNaznNonObligatoryCurrencySellingKeyword()))) {
        return true;
      }
      return false;
    };
  }
}
