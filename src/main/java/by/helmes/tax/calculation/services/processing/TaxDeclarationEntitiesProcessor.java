package by.helmes.tax.calculation.services.processing;

import by.helmes.tax.calculation.models.output.Quarter;
import by.helmes.tax.calculation.models.output.TaxDeclarationEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by aliaksei.melnikau on 29.03.16.
 */
@Component
public class TaxDeclarationEntitiesProcessor {

    public List<List<TaxDeclarationEntity>> splitTaxDeclarationEntitiesByQuarters(List<TaxDeclarationEntity> taxDeclarationEntities) {
        List<List<TaxDeclarationEntity>> groupedEntities = new ArrayList<>();
        if (taxDeclarationEntities.size() == 0) {
            return groupedEntities;
        }
        Collections.sort(taxDeclarationEntities);
        int year = taxDeclarationEntities.get(0).getYear();
        Quarter quarter = Quarter.getQuarter(taxDeclarationEntities.get(0).getMonth());

        List<TaxDeclarationEntity> entitiesOfTheSameQuarter = new ArrayList<>();
        for (TaxDeclarationEntity taxDeclarationEntity : taxDeclarationEntities) {
            if (taxDeclarationEntity.getYear()!=year || quarter!=Quarter.getQuarter(taxDeclarationEntity.getMonth())) {
                groupedEntities.add(entitiesOfTheSameQuarter);

                entitiesOfTheSameQuarter = new ArrayList<>();
                entitiesOfTheSameQuarter.add(taxDeclarationEntity);
                year = taxDeclarationEntity.getYear();
                quarter = Quarter.getQuarter(taxDeclarationEntity.getMonth());
            } else {
                entitiesOfTheSameQuarter.add(taxDeclarationEntity);
            }
        }
        groupedEntities.add(entitiesOfTheSameQuarter);


        return groupedEntities;
    }
}
