package by.helmes.tax.calculation.services.output;

import by.helmes.tax.calculation.models.output.Section1Part1Entity;
import by.helmes.tax.calculation.models.output.TaxDeclarationEntity;
import by.helmes.tax.calculation.services.processing.LedgerEntitiesProcessor;
import by.helmes.tax.calculation.utils.xls.declaration.TaxDeclarationXlsUtils;
import by.helmes.tax.calculation.utils.xls.ledger.LedgerSection1Part1XlsUtils;
import by.helmes.tax.calculation.utils.xls.ledger.LedgerSection1Part2XlsUtils;
import by.helmes.tax.calculation.utils.props.PropertiesResolver;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

/**
 * @author petr
 */
@Component
public class XlsExporter {

    @Autowired
    private PropertiesResolver propertiesResolver;

    @Autowired
    private LedgerEntitiesProcessor ledgerEntitiesProcessor;

    @Autowired
    private LedgerSection1Part1XlsUtils ledgerSection1Part1XlsUtils;

    @Autowired
    private LedgerSection1Part2XlsUtils ledgerSection1Part2XlsUtils;

    @Autowired
    private TaxDeclarationXlsUtils taxDeclarationXlsUtils;

    public static final String XLS_TEMPLATES_FOLDER = "xls";

    public Workbook fillLedger(List<Section1Part1Entity> newEntriesSection1Part1, InputStream existedLedgerInputStream) throws IOException {
        HSSFWorkbook ledger = null;
        if (existedLedgerInputStream == null) {
            ledger = new HSSFWorkbook(getResourceAsInputStream(XLS_TEMPLATES_FOLDER + File.separator + propertiesResolver.getLedgerFilename()));
        } else {
            ledger = new HSSFWorkbook(existedLedgerInputStream);
        }
        Collections.sort(newEntriesSection1Part1);
        HSSFSheet section1Part1Sheet = ledger.getSheet(propertiesResolver.getLedgerSection1Part1SheetName());
        ledgerSection1Part1XlsUtils.writeSection1Part1Entities(section1Part1Sheet, newEntriesSection1Part1);

        HSSFSheet section1Part2Sheet = ledger.getSheet(propertiesResolver.getLedgerSection1Part2SheetName());
        ledgerSection1Part2XlsUtils.writeSection1Part2Entities(section1Part2Sheet, ledgerEntitiesProcessor.toSection1Part2Entities(newEntriesSection1Part1));
        return ledger;
    }

    public Workbook createTaxReport(List<TaxDeclarationEntity> taxableEntities) throws IOException {
        HSSFWorkbook taxReport = new HSSFWorkbook(getResourceAsInputStream(XLS_TEMPLATES_FOLDER + File.separator + propertiesResolver.getTaxDeclarationFilename()));
        Collections.sort(taxableEntities);

        taxDeclarationXlsUtils.fillTaxReport(taxReport, taxableEntities);

        return taxReport;
    }

    private InputStream getResourceAsInputStream(String path) {
        InputStream resource = XlsExporter.class.getClassLoader().getResourceAsStream(path);
        if (resource == null) {
            path = path.replace("\\","/"); // File.separator might resolve into wrong symbol when application runs as Jar on Windows machine
        }
        resource = XlsExporter.class.getClassLoader().getResourceAsStream(path);
        if (resource == null) {
            throw new IllegalArgumentException("Cannot load file from path: "+path);
        }

        return resource;
    }
}
