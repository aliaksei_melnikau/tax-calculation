package by.helmes.tax.calculation.models.output;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * Entity which represents a single entry in ledger book on sheet "Раздел I ч.II п.1"
 *
 * @author aliaksei.melnikau
 */
public class Section1Part2Entity implements Comparable<Section1Part2Entity>{
    private LocalDate shipmentDate;
    private String productReceiver;
    private BigDecimal amountByr;
    private String currencyName;
    private BigDecimal amountCurrency;
    private List<Invoice> invoices;

    public LocalDate getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(LocalDate shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public String getProductReceiver() {
        return productReceiver;
    }

    public void setProductReceiver(String productReceiver) {
        this.productReceiver = productReceiver;
    }

    public BigDecimal getAmountByr() {
        return amountByr;
    }

    public void setAmountByr(BigDecimal amountByr) {
        this.amountByr = amountByr;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public BigDecimal getAmountCurrency() {
        return amountCurrency;
    }

    public void setAmountCurrency(BigDecimal amountCurrency) {
        this.amountCurrency = amountCurrency;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section1Part2Entity that = (Section1Part2Entity) o;
        return Objects.equals(shipmentDate, that.shipmentDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shipmentDate);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("shipmentDate", shipmentDate)
                .add("productReceiver", productReceiver)
                .add("amountByr", amountByr)
                .add("currencyName", currencyName)
                .add("amountCurrency", amountCurrency)
                .add("invoices", invoices)
                .toString();
    }

    @Override
    public int compareTo(Section1Part2Entity that) {
        return this.getShipmentDate().compareTo(that.getShipmentDate());
    }

    public static class Invoice {
        private LocalDate paymentDate;
        private String documentName;
        private BigDecimal amountByr;

        public LocalDate getPaymentDate() {
            return paymentDate;
        }

        public void setPaymentDate(LocalDate paymentDate) {
            this.paymentDate = paymentDate;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public BigDecimal getAmountByr() {
            return amountByr;
        }

        public void setAmountByr(BigDecimal amountByr) {
            this.amountByr = amountByr;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Invoice invoice = (Invoice) o;
            return Objects.equals(paymentDate, invoice.paymentDate) &&
                    Objects.equals(documentName, invoice.documentName) &&
                    Objects.equals(amountByr, invoice.amountByr);
        }

        @Override
        public int hashCode() {
            return Objects.hash(paymentDate, documentName, amountByr);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("paymentDate", paymentDate)
                    .add("documentName", documentName)
                    .add("amountByr", amountByr)
                    .toString();
        }
    }

}
