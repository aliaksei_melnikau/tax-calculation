package by.helmes.tax.calculation.models.output;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.math.BigDecimal;
import java.time.Month;
import java.util.Objects;

/**
 *
 * Entity which represents revenue data for a single month
 *
 * @author aliaksei.melnikau
 */
public class TaxDeclarationEntity implements Comparable<TaxDeclarationEntity>{
    private BigDecimal taxBase = BigDecimal.ZERO;
    private BigDecimal nonSaleRevenue = BigDecimal.ZERO;
    private int year;
    private Month month;

    public TaxDeclarationEntity(int year, Month month) {
        this.year = year;
        this.month = month;
    }

    public BigDecimal getTaxBase() {
        return taxBase;
    }

    public void setTaxBase(BigDecimal taxBase) {
        this.taxBase = taxBase;
    }

    public BigDecimal getNonSaleRevenue() {
        return nonSaleRevenue;
    }

    public void setNonSaleRevenue(BigDecimal nonSaleRevenue) {
        this.nonSaleRevenue = nonSaleRevenue;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaxDeclarationEntity that = (TaxDeclarationEntity) o;
        return Objects.equals(year, that.year) &&
                Objects.equals(month, that.month);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("year", year)
                .add("month", month)
                .add("taxBase", taxBase)
                .add("nonSaleRevenue", nonSaleRevenue)
                .toString();
    }

    @Override
    public int compareTo(TaxDeclarationEntity that) {
        return ComparisonChain.start()
                .compare(this.year, that.year, Ordering.natural())
                .compare(this.month, that.month, Ordering.natural())
                .result();
    }
}
