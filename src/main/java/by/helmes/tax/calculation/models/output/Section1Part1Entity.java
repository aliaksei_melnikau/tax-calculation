package by.helmes.tax.calculation.models.output;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * Entity which represents a single entry in ledger book on sheet "Раздел I ч.I"
 *
 * @author petr
 */
public class Section1Part1Entity implements Comparable<Section1Part1Entity>{
	private RevenueType type;
	private LocalDate date;
	private BigDecimal amountByr;
    private BigDecimal amountCurrency;
	private String documentName;
	private String operationName;

	public Section1Part1Entity() {
	}

	public Section1Part1Entity(RevenueType type, LocalDate date, BigDecimal amountByr) {
		this.type = type;
		this.date = date;
		this.amountByr = amountByr;
    }

	public RevenueType getType() {
		return type;
	}

	public void setType(RevenueType type) {
		this.type = type;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public BigDecimal getAmountByr() {
		return amountByr;
	}

	public void setAmountByr(BigDecimal amountByr) {
		this.amountByr = amountByr;
	}

    public BigDecimal getAmountCurrency() {
        return amountCurrency;
    }

    public void setAmountCurrency(BigDecimal amountCurrency) {
        this.amountCurrency = amountCurrency;
    }

    public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public static enum RevenueType {
		NON_SALE, SALARY
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Section1Part1Entity section1Part1Entity = (Section1Part1Entity) o;
		return Objects.equals(type, section1Part1Entity.type) &&
				Objects.equals(date, section1Part1Entity.date) &&
				Objects.equals(amountByr, section1Part1Entity.amountByr);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, date, amountByr);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("type", type)
				.add("date", date)
				.add("amountByr", amountByr)
                .add("amountCurrency", amountCurrency)
				.add("documentName", documentName)
				.add("operationName", operationName)
				.toString();
	}

	@Override
    public int compareTo(Section1Part1Entity o) {
	    return this.getDate().compareTo(o.getDate());
	}

}
