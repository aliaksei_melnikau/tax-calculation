package by.helmes.tax.calculation.models.payment;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by aliaksei.melnikau on 1/29/2016.
 */
public class ByrPayment extends BasePayment {
    private Long docSourceId;
    private BigDecimal credit;

    public Long getDocSourceId() {
        return docSourceId;
    }

    public void setDocSourceId(Long docSourceId) {
        this.docSourceId = docSourceId;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ByrPayment that = (ByrPayment) o;
        return Objects.equals(docSourceId, that.docSourceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(docSourceId);
    }

    @Override
    protected MoreObjects.ToStringHelper toStringHelper() {
        return super.toStringHelper()
                .add("docSourceId", this.docSourceId)
                .add("credit",this.credit);
    }
}
