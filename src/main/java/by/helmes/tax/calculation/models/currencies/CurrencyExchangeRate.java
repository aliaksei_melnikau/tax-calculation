package by.helmes.tax.calculation.models.currencies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Model object for response returned by National Bank API when exchange rate is requested
 *
 * @author Aliaksei Melnikau (extmua@audatex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyExchangeRate {
  @JsonProperty("Cur_ID")
  private String currencyId;
  @JsonProperty("Date")
  private LocalDate date;
  @JsonProperty("Cur_Abbreviation")
  private String currencyAbbreviation;
  @JsonProperty("Cur_Scale")
  private long currencyScale;
  @JsonProperty("Cur_Name")
  private String currencyName;
  @JsonProperty("Cur_OfficialRate")
  private BigDecimal rateToByn;

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getCurrencyAbbreviation() {
    return currencyAbbreviation;
  }

  public void setCurrencyAbbreviation(String currencyAbbreviation) {
    this.currencyAbbreviation = currencyAbbreviation;
  }

  public long getCurrencyScale() {
    return currencyScale;
  }

  public void setCurrencyScale(long currencyScale) {
    this.currencyScale = currencyScale;
  }

  public String getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }

  public BigDecimal getRateToByn() {
    return rateToByn;
  }

  public void setRateToByn(BigDecimal rateToByn) {
    this.rateToByn = rateToByn;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CurrencyExchangeRate that = (CurrencyExchangeRate) o;
    return currencyScale == that.currencyScale &&
            Objects.equals(currencyId, that.currencyId) &&
            Objects.equals(date, that.date) &&
            Objects.equals(currencyAbbreviation, that.currencyAbbreviation) &&
            Objects.equals(currencyName, that.currencyName) &&
            Objects.equals(rateToByn, that.rateToByn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currencyId, date, currencyAbbreviation, currencyScale, currencyName, rateToByn);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
            .add("currencyId", currencyId)
            .add("date", date)
            .add("currencyAbbreviation", currencyAbbreviation)
            .add("currencyScale", currencyScale)
            .add("currencyName", currencyName)
            .add("rateToByn", rateToByn)
            .toString();
  }
}
