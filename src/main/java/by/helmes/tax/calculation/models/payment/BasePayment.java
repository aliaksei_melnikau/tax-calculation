package by.helmes.tax.calculation.models.payment;

import com.google.common.base.MoreObjects;

import java.time.LocalDate;

/**
 * Created by aliaksei.melnikau on 1/29/2016.
 */
public abstract class BasePayment {
    private LocalDate docDate;
    private String korName;
    private String nazn;

    public LocalDate getDocDate() {
        return docDate;
    }

    public void setDocDate(LocalDate docDate) {
        this.docDate = docDate;
    }

    public String getKorName() {
        return korName;
    }

    public void setKorName(String korName) {
        this.korName = korName;
    }

    public String getNazn() {
        return nazn;
    }

    public void setNazn(String nazn) {
        this.nazn = nazn;
    }

    protected MoreObjects.ToStringHelper toStringHelper() {
        return MoreObjects.toStringHelper(this)
                .add("docDate", docDate)
                .add("korName", korName)
                .add("nazn", nazn);
    }

    @Override
    public final String toString() {
        return toStringHelper().toString();
    }
}
