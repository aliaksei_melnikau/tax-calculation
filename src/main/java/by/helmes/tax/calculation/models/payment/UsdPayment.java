package by.helmes.tax.calculation.models.payment;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by aliaksei.melnikau on 1/29/2016.
 */
public class UsdPayment extends BasePayment {
    private BigDecimal deb;
    private BigDecimal cre;
    private BigDecimal creQ;
    private BigDecimal debQ;
    private Long inDocId;

    public BigDecimal getDeb() {
        return deb;
    }

    public void setDeb(BigDecimal deb) {
        this.deb = deb;
    }

    public BigDecimal getCre() {
        return cre;
    }

    public void setCre(BigDecimal cre) {
        this.cre = cre;
    }

    public BigDecimal getCreQ() {
        return creQ;
    }

    public void setCreQ(BigDecimal creQ) {
        this.creQ = creQ;
    }

    public BigDecimal getDebQ() {
        return debQ;
    }

    public void setDebQ(BigDecimal debQ) {
        this.debQ = debQ;
    }

    public Long getInDocId() {
        return inDocId;
    }

    public void setInDocId(Long inDocId) {
        this.inDocId = inDocId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsdPayment that = (UsdPayment) o;
        return Objects.equals(inDocId, that.inDocId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inDocId);
    }

    @Override
    protected MoreObjects.ToStringHelper toStringHelper() {
        return super.toStringHelper()
                .add("deb", deb)
                .add("cre", cre)
                .add("creQ", creQ)
                .add("debQ", debQ)
                .add("inDocId", inDocId);
    }

}
