package by.helmes.tax.calculation.models.output;

import java.time.Month;
import java.util.Arrays;
import java.util.List;

public enum Quarter {
    I, II, III, IV;

    public Quarter getNextQuarter() {
        switch (this) {
            case I:
                return II;
            case II:
                return III;
            case III:
                return IV;
            default:
                throw new IllegalArgumentException("Only four quarters are supported");
        }
    }

    public List<Month> getMatchingMonths() {
        switch (this) {
            case I:
                return Arrays.asList(Month.JANUARY, Month.FEBRUARY, Month.MARCH);
            case II:
                return Arrays.asList(Month.APRIL, Month.MAY, Month.JUNE);
            case III:
                return Arrays.asList(Month.JULY, Month.AUGUST,Month.SEPTEMBER);
            case IV:
                return Arrays.asList(Month.OCTOBER,Month.NOVEMBER,Month.DECEMBER);
            default:
                throw new IllegalArgumentException("Only four quarters are supported");
        }
    }

    public static Quarter getQuarter(Month month) {
        switch (month) {
            case JANUARY:
                return I;
            case FEBRUARY:
                return I;
            case MARCH:
                return I;
            case APRIL:
                return II;
            case MAY:
                return II;
            case JUNE:
                return II;
            case JULY:
                return III;
            case AUGUST:
                return III;
            case SEPTEMBER:
                return III;
            case OCTOBER:
                return IV;
            case NOVEMBER:
                return IV;
            case DECEMBER:
                return IV;
            default:
                throw new RuntimeException("Month cannot be null");
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case I:
                return "I";
            case II:
                return "II";
            case III:
                return "III";
            case IV:
                return "IV";
            default:
                throw new IllegalArgumentException("Only four quarters are supported");
        }
    }
}